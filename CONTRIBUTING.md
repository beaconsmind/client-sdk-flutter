# Contributing

## Upgrading native SDKs

Before starting with the upgrade process, make sure that both iOS & Android example apps are in the buildable and working state. See [Development workflow](#development-workflow) section to learn how to run the app.

### iOS

1. Check what is the latest version of the iOS SDK [here](https://gitlab.com/beaconsmind/client-sdk-ios/-/releases).
2. Update [ios/beaconsmind_sdk.podspec](ios/beaconsmind_sdk.podspec) to point to the new version:
   ```diff
   -  s.dependency "Beaconsmind", "1.0.0"
   +  s.dependency "Beaconsmind", "1.0.1"
   ```
3. Install updated dependency:
   ```
   cd example/ios
   pod update Beaconsmind
   cd ../..
   ```
4. Open the example app in Xcode and build the app to see if compiles successfully.
   ```
   open example/ios/Runner.xcworkspace   
   ```
5. Check what are the changes from the current version that the Flutter SDK is using and the latest version. If needed, speak with the developer responsible for the native platform. If there are new methods that need to be exposed via Flutter binding, make sure to implement them.
6. Run the iOS example app to make sure that everything works correctly after the upgrade.
7. Make sure to test the new bindings if needed.
8. Make sure to update the [README.md](README.MD) if needed.

### Android

1. Check what is the latest version of the Android SDK [here](https://gitlab.com/beaconsmind/client-sdk-android/-/releases).
2. Update [android/build.gradle](android/build.gradle) to point to the new version:
   ```diff
   -  implementation "com.beaconsmind:client-sdk-android:1.0.0"
   +  implementation "com.beaconsmind:client-sdk-android:1.0.1"
   ```
3. Open the example app in Android Studio.
   ```
   open -a /Applications/Android\ Studio.app example/android 
   ```
4. Run the gradle sync and build the project to see if compiles successfully.
5. Check what are the changes from the current version that the Flutter SDK is using and the latest version. If needed, speak with the developer responsible for the native platform. If there are new methods that need to be exposed via Flutter binding, make sure to implement them.
6. Run the Android example app to make sure that everything works correctly after the upgrade.
7. Make sure to test the new bindings if needed.
8. Make sure to update the [README.md](README.MD) if needed.

## Development workflow

To get started with the project:
1. Install [Flutter Version Manager](https://fvm.app/).
2. Run `fvm install` in the root project directory to install the Flutter version required by the project.
3. Run `fvm flutter packages get` in the root directory to install the required dependencies for each package.

While developing, you can run the [example app](/example/) to test your changes. Any changes you make in your library's Dart code will be reflected in the example app without a rebuild. If you change any native code, then you'll need to rebuild the example app.

To edit the Swift files, run `open example/ios/Runner.xcworkspace` and find the source files at `Pods/Development Pods/beaconsmind_sdk/../../example/ios/.symlinks/plugins/beaconsmind_sdk/ios/Classes`.
To edit the Kotlin files, run `open -a /Applications/Android\ Studio.app example/android` and find the source files at `android/beaconsmind_sdk`.

For more details, visit [Developing packages & plugins](https://docs.flutter.dev/packages-and-plugins/developing-packages).

To run the example app:

```sh
cd example
fvm flutter run
```

To run the example app in a release configuration:

```sh
fvm flutter run --release
```

Make sure your code passes Dart analysis. Run the following to verify:

```sh
fvm dart analyze
```

To fix formatting errors, run the following:

```sh
fvm dart format
```

Remember to add tests for your change if possible. Run the unit tests by:

```sh
fvm dart test
```

### Commit message convention

We follow the [conventional commits specification](https://www.conventionalcommits.org/en) for our commit messages:

- `fix`: bug fixes, e.g. fix crash due to deprecated method.
- `feat`: new features, e.g. add new method to the module.
- `build`: change in build system or external dependencies.
- `refactor`: code refactor, e.g. migrate from class components to hooks.
- `docs`: changes into documentation, e.g. add usage example for the module.
- `test`: adding or updating tests, e.g. add integration tests using detox.
- `chore`: tooling changes, e.g. change CI config.

### Sending a pull request

When you're sending a pull request:

- Prefer small pull requests focused on one change.
- Verify that linters and tests are passing.
- Review the documentation to make sure it looks good.
- Follow the pull request template when opening a pull request.
- For pull requests that change the API or implementation, discuss with maintainers first by opening an issue.

## Releasing new version

Before starting with the releasing process, make sure that you are on the lastest main branch.

1. Run `tool/create_release.sh`. It will:
   1. generate release notes and changelog,
   2. commit all the change, create tag and push to remote.
   
   **Make sure to install all the required dependencies, they are listed at the top of the script file.**

2. Go to project's GitLab repository and create a release from the newly created tag.
3. Run `tool/publish_package.sh`. It will publish the dart package to https://pub.dev.
4. Run `tool/publish_example_app.sh`. It will:
   1. Build the APK & IPA files.
   2. Send iOS IPA file to TestFlight using fastlane.
   3. TODO: Send APK file.
   
   **Make sure to fill in all the required credentials in the script file.**
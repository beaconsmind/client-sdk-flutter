import 'package:beaconsmind_sdk/beaconsmind_sdk.dart';
import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  TestWidgetsFlutterBinding.ensureInitialized();

  group('Beaconsmind', () {
    group('initialize', () {
      final log = <MethodCall>[];

      setUp(() {
        log.clear();
        TestDefaultBinaryMessengerBinding.instance.defaultBinaryMessenger
            .setMockMethodCallHandler(
          const MethodChannel('beaconsmind/methods'),
          (call) async {
            log.add(call);
            switch (call.method) {
              case 'initialize':
                return true;
              default:
                return null;
            }
          },
        );
      });

      test('should call initialize with correct arguments', () async {
        await Beaconsmind.instance.initialize(
          hostname: 'https://adidas.bms.beaconsmind.com',
          appVersion: '1.0.0',
        );

        expect(log, hasLength(1));
        expect(log.first.method, 'initialize');
        expect(
          log.first.arguments['hostname'],
          equals('https://adidas.bms.beaconsmind.com'),
        );
        expect(
          log.first.arguments['appVersion'],
          equals('1.0.0'),
        );
      });
    });

    group('initializeDevelop', () {
      final log = <MethodCall>[];

      setUp(() {
        log.clear();

        TestDefaultBinaryMessengerBinding.instance.defaultBinaryMessenger
            .setMockMethodCallHandler(
          const MethodChannel('beaconsmind/methods'),
          (call) async {
            log.add(call);
            switch (call.method) {
              case 'initializeDevelop':
                return true;
              default:
                return null;
            }
          },
        );
      });

      test('should call initializeDevelop with correct arguments', () async {
        await Beaconsmind.instance.initializeDevelop(
          hostname: 'https://adidas.bms.beaconsmind.com',
          appVersion: '1.0.0',
        );

        expect(log, hasLength(1));
        expect(log.first.method, 'initializeDevelop');
        expect(
          log.first.arguments['hostname'],
          equals('https://adidas.bms.beaconsmind.com'),
        );
        expect(
          log.first.arguments['appVersion'],
          equals('1.0.0'),
        );
      });
    });

    group('setMinLogLevel', () {
      final log = <MethodCall>[];

      setUp(() {
        log.clear();

        TestDefaultBinaryMessengerBinding.instance.defaultBinaryMessenger
            .setMockMethodCallHandler(
          const MethodChannel('beaconsmind/methods'),
          (call) async {
            log.add(call);
            switch (call.method) {
              case 'setMinLogLevel':
                return true;
              default:
                return null;
            }
          },
        );
      });

      test('should call setMinLogLevel with correct arguments', () async {
        await Beaconsmind.instance.setMinLogLevel(LogLevel.info);

        expect(log, hasLength(1));
        expect(log.first.method, 'setMinLogLevel');
        expect(log.first.arguments, {
          'minLogLevel': 'info',
        });
      });
    });
  });
}

import 'package:beaconsmind_sdk/src/models/log/log.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  group('LogLevel', () {
    group('parseToString', () {
      test(
          'returns correct string value '
          'when enum value is debug', () {
        expect(LogLevel.debug.parseToString(), 'debug');
      });

      test(
          'returns correct string value '
          'when enum value is info', () {
        expect(LogLevel.info.parseToString(), 'info');
      });

      test(
          'returns correct string value '
          'when enum value is warning', () {
        expect(LogLevel.warning.parseToString(), 'warning');
      });

      test(
          'returns correct string value '
          'when enum value is error', () {
        expect(LogLevel.error.parseToString(), 'error');
      });

      test(
          'returns correct string value '
          'when enum value is silent', () {
        expect(LogLevel.silent.parseToString(), 'silent');
      });
    });
  });
}

import 'package:beaconsmind_sdk/src/models/beacon_contacts_summary/beacon_contacts_summary.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  group('BeaconContactsSummary', () {
    test('can be instantiated from json', () {
      final json = {
        'uuid': '2F234454-CF6D-4A0F-ADF2-F4911BA9FFA6',
        'major': '1',
        'minor': '2',
        'name': 'Cool Beacon',
        'store': 'Awesome Store',
        'currentRSSI': 1.0,
        'averageRSSI': 1.0,
        'lastContactTimestamp': DateTime(1970).millisecondsSinceEpoch,
        'contactFrequencyPerMinute': 1,
        'contactsCount': 60,
        'isInRange': true,
      };

      expect(
        BeaconContactsSummary.fromJson(json),
        equals(
          BeaconContactsSummary(
            uuid: '2F234454-CF6D-4A0F-ADF2-F4911BA9FFA6',
            major: '1',
            minor: '2',
            name: 'Cool Beacon',
            store: 'Awesome Store',
            currentRSSI: 1.0,
            averageRSSI: 1.0,
            lastContactTimestamp: DateTime(1970).millisecondsSinceEpoch,
            contactFrequencyPerMinute: 1,
            contactsCount: 60,
            isInRange: true,
          ),
        ),
      );
    });
  });
}

import 'package:beaconsmind_sdk/beaconsmind_sdk.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  TestWidgetsFlutterBinding.ensureInitialized();

  test('Beaconsmind singleton instance test', () {
    expect(Beaconsmind.instance, Beaconsmind.instance);
  });
}

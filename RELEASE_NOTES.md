#  (2023-11-07)


### Features

* add platformType parameter to registerDeviceToken method [5447603697] ([e0dbba9](https://gitlab.com/beaconsmind/client-sdk-flutter/commit/e0dbba9b8b7d3da7fb9de67c7d94221108c289a2))




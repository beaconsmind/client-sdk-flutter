// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'beacon.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

Beacon _$BeaconFromJson(Map<String, dynamic> json) {
  return _Beacon.fromJson(json);
}

/// @nodoc
class _$BeaconTearOff {
  const _$BeaconTearOff();

  _Beacon call(
      {required String uuid,
      required String major,
      required String minor,
      required String name,
      required String store}) {
    return _Beacon(
      uuid: uuid,
      major: major,
      minor: minor,
      name: name,
      store: store,
    );
  }

  Beacon fromJson(Map<String, Object?> json) {
    return Beacon.fromJson(json);
  }
}

/// @nodoc
const $Beacon = _$BeaconTearOff();

/// @nodoc
mixin _$Beacon {
  String get uuid => throw _privateConstructorUsedError;
  String get major => throw _privateConstructorUsedError;
  String get minor => throw _privateConstructorUsedError;
  String get name => throw _privateConstructorUsedError;
  String get store => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $BeaconCopyWith<Beacon> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $BeaconCopyWith<$Res> {
  factory $BeaconCopyWith(Beacon value, $Res Function(Beacon) then) =
      _$BeaconCopyWithImpl<$Res>;
  $Res call(
      {String uuid, String major, String minor, String name, String store});
}

/// @nodoc
class _$BeaconCopyWithImpl<$Res> implements $BeaconCopyWith<$Res> {
  _$BeaconCopyWithImpl(this._value, this._then);

  final Beacon _value;
  // ignore: unused_field
  final $Res Function(Beacon) _then;

  @override
  $Res call({
    Object? uuid = freezed,
    Object? major = freezed,
    Object? minor = freezed,
    Object? name = freezed,
    Object? store = freezed,
  }) {
    return _then(_value.copyWith(
      uuid: uuid == freezed
          ? _value.uuid
          : uuid // ignore: cast_nullable_to_non_nullable
              as String,
      major: major == freezed
          ? _value.major
          : major // ignore: cast_nullable_to_non_nullable
              as String,
      minor: minor == freezed
          ? _value.minor
          : minor // ignore: cast_nullable_to_non_nullable
              as String,
      name: name == freezed
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      store: store == freezed
          ? _value.store
          : store // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
abstract class _$BeaconCopyWith<$Res> implements $BeaconCopyWith<$Res> {
  factory _$BeaconCopyWith(_Beacon value, $Res Function(_Beacon) then) =
      __$BeaconCopyWithImpl<$Res>;
  @override
  $Res call(
      {String uuid, String major, String minor, String name, String store});
}

/// @nodoc
class __$BeaconCopyWithImpl<$Res> extends _$BeaconCopyWithImpl<$Res>
    implements _$BeaconCopyWith<$Res> {
  __$BeaconCopyWithImpl(_Beacon _value, $Res Function(_Beacon) _then)
      : super(_value, (v) => _then(v as _Beacon));

  @override
  _Beacon get _value => super._value as _Beacon;

  @override
  $Res call({
    Object? uuid = freezed,
    Object? major = freezed,
    Object? minor = freezed,
    Object? name = freezed,
    Object? store = freezed,
  }) {
    return _then(_Beacon(
      uuid: uuid == freezed
          ? _value.uuid
          : uuid // ignore: cast_nullable_to_non_nullable
              as String,
      major: major == freezed
          ? _value.major
          : major // ignore: cast_nullable_to_non_nullable
              as String,
      minor: minor == freezed
          ? _value.minor
          : minor // ignore: cast_nullable_to_non_nullable
              as String,
      name: name == freezed
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      store: store == freezed
          ? _value.store
          : store // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_Beacon implements _Beacon {
  const _$_Beacon(
      {required this.uuid,
      required this.major,
      required this.minor,
      required this.name,
      required this.store});

  factory _$_Beacon.fromJson(Map<String, dynamic> json) =>
      _$$_BeaconFromJson(json);

  @override
  final String uuid;
  @override
  final String major;
  @override
  final String minor;
  @override
  final String name;
  @override
  final String store;

  @override
  String toString() {
    return 'Beacon(uuid: $uuid, major: $major, minor: $minor, name: $name, store: $store)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _Beacon &&
            const DeepCollectionEquality().equals(other.uuid, uuid) &&
            const DeepCollectionEquality().equals(other.major, major) &&
            const DeepCollectionEquality().equals(other.minor, minor) &&
            const DeepCollectionEquality().equals(other.name, name) &&
            const DeepCollectionEquality().equals(other.store, store));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(uuid),
      const DeepCollectionEquality().hash(major),
      const DeepCollectionEquality().hash(minor),
      const DeepCollectionEquality().hash(name),
      const DeepCollectionEquality().hash(store));

  @JsonKey(ignore: true)
  @override
  _$BeaconCopyWith<_Beacon> get copyWith =>
      __$BeaconCopyWithImpl<_Beacon>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_BeaconToJson(this);
  }
}

abstract class _Beacon implements Beacon {
  const factory _Beacon(
      {required String uuid,
      required String major,
      required String minor,
      required String name,
      required String store}) = _$_Beacon;

  factory _Beacon.fromJson(Map<String, dynamic> json) = _$_Beacon.fromJson;

  @override
  String get uuid;
  @override
  String get major;
  @override
  String get minor;
  @override
  String get name;
  @override
  String get store;
  @override
  @JsonKey(ignore: true)
  _$BeaconCopyWith<_Beacon> get copyWith => throw _privateConstructorUsedError;
}

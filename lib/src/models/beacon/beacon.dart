import 'package:freezed_annotation/freezed_annotation.dart';

part 'beacon.freezed.dart';

part 'beacon.g.dart';

@freezed
class Beacon with _$Beacon {
  const factory Beacon({
    required String uuid,
    required String major,
    required String minor,
    required String name,
    required String store,
  }) = _Beacon;

  factory Beacon.fromJson(Map<String, dynamic> json) => _$BeaconFromJson(json);
}

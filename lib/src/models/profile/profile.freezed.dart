// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'profile.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

Profile _$ProfileFromJson(Map<String, dynamic> json) {
  return _Profile.fromJson(json);
}

/// @nodoc
class _$ProfileTearOff {
  const _$ProfileTearOff();

  _Profile call(
      {required bool disablePushNotifications,
      required String firstName,
      required DateTime joinDate,
      required String lastName,
      required bool newsLetterSubscription,
      DateTime? birthDate,
      String? city,
      List<String>? claims,
      String? clubId,
      String? country,
      String? favoriteStore,
      int? favoriteStoreId,
      String? fullName,
      String? gender,
      String? houseNumber,
      String? id,
      String? landlinePhone,
      String? language,
      String? phoneNumber,
      List<String>? roles,
      String? street,
      String? url,
      String? userName,
      String? zipCode}) {
    return _Profile(
      disablePushNotifications: disablePushNotifications,
      firstName: firstName,
      joinDate: joinDate,
      lastName: lastName,
      newsLetterSubscription: newsLetterSubscription,
      birthDate: birthDate,
      city: city,
      claims: claims,
      clubId: clubId,
      country: country,
      favoriteStore: favoriteStore,
      favoriteStoreId: favoriteStoreId,
      fullName: fullName,
      gender: gender,
      houseNumber: houseNumber,
      id: id,
      landlinePhone: landlinePhone,
      language: language,
      phoneNumber: phoneNumber,
      roles: roles,
      street: street,
      url: url,
      userName: userName,
      zipCode: zipCode,
    );
  }

  Profile fromJson(Map<String, Object?> json) {
    return Profile.fromJson(json);
  }
}

/// @nodoc
const $Profile = _$ProfileTearOff();

/// @nodoc
mixin _$Profile {
  bool get disablePushNotifications => throw _privateConstructorUsedError;
  String get firstName => throw _privateConstructorUsedError;
  DateTime get joinDate => throw _privateConstructorUsedError;
  String get lastName => throw _privateConstructorUsedError;
  bool get newsLetterSubscription => throw _privateConstructorUsedError;
  DateTime? get birthDate => throw _privateConstructorUsedError;
  String? get city => throw _privateConstructorUsedError;
  List<String>? get claims => throw _privateConstructorUsedError;
  String? get clubId => throw _privateConstructorUsedError;
  String? get country => throw _privateConstructorUsedError;
  String? get favoriteStore => throw _privateConstructorUsedError;
  int? get favoriteStoreId => throw _privateConstructorUsedError;
  String? get fullName => throw _privateConstructorUsedError;
  String? get gender => throw _privateConstructorUsedError;
  String? get houseNumber => throw _privateConstructorUsedError;
  String? get id => throw _privateConstructorUsedError;
  String? get landlinePhone => throw _privateConstructorUsedError;
  String? get language => throw _privateConstructorUsedError;
  String? get phoneNumber => throw _privateConstructorUsedError;
  List<String>? get roles => throw _privateConstructorUsedError;
  String? get street => throw _privateConstructorUsedError;
  String? get url => throw _privateConstructorUsedError;
  String? get userName => throw _privateConstructorUsedError;
  String? get zipCode => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $ProfileCopyWith<Profile> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ProfileCopyWith<$Res> {
  factory $ProfileCopyWith(Profile value, $Res Function(Profile) then) =
      _$ProfileCopyWithImpl<$Res>;
  $Res call(
      {bool disablePushNotifications,
      String firstName,
      DateTime joinDate,
      String lastName,
      bool newsLetterSubscription,
      DateTime? birthDate,
      String? city,
      List<String>? claims,
      String? clubId,
      String? country,
      String? favoriteStore,
      int? favoriteStoreId,
      String? fullName,
      String? gender,
      String? houseNumber,
      String? id,
      String? landlinePhone,
      String? language,
      String? phoneNumber,
      List<String>? roles,
      String? street,
      String? url,
      String? userName,
      String? zipCode});
}

/// @nodoc
class _$ProfileCopyWithImpl<$Res> implements $ProfileCopyWith<$Res> {
  _$ProfileCopyWithImpl(this._value, this._then);

  final Profile _value;
  // ignore: unused_field
  final $Res Function(Profile) _then;

  @override
  $Res call({
    Object? disablePushNotifications = freezed,
    Object? firstName = freezed,
    Object? joinDate = freezed,
    Object? lastName = freezed,
    Object? newsLetterSubscription = freezed,
    Object? birthDate = freezed,
    Object? city = freezed,
    Object? claims = freezed,
    Object? clubId = freezed,
    Object? country = freezed,
    Object? favoriteStore = freezed,
    Object? favoriteStoreId = freezed,
    Object? fullName = freezed,
    Object? gender = freezed,
    Object? houseNumber = freezed,
    Object? id = freezed,
    Object? landlinePhone = freezed,
    Object? language = freezed,
    Object? phoneNumber = freezed,
    Object? roles = freezed,
    Object? street = freezed,
    Object? url = freezed,
    Object? userName = freezed,
    Object? zipCode = freezed,
  }) {
    return _then(_value.copyWith(
      disablePushNotifications: disablePushNotifications == freezed
          ? _value.disablePushNotifications
          : disablePushNotifications // ignore: cast_nullable_to_non_nullable
              as bool,
      firstName: firstName == freezed
          ? _value.firstName
          : firstName // ignore: cast_nullable_to_non_nullable
              as String,
      joinDate: joinDate == freezed
          ? _value.joinDate
          : joinDate // ignore: cast_nullable_to_non_nullable
              as DateTime,
      lastName: lastName == freezed
          ? _value.lastName
          : lastName // ignore: cast_nullable_to_non_nullable
              as String,
      newsLetterSubscription: newsLetterSubscription == freezed
          ? _value.newsLetterSubscription
          : newsLetterSubscription // ignore: cast_nullable_to_non_nullable
              as bool,
      birthDate: birthDate == freezed
          ? _value.birthDate
          : birthDate // ignore: cast_nullable_to_non_nullable
              as DateTime?,
      city: city == freezed
          ? _value.city
          : city // ignore: cast_nullable_to_non_nullable
              as String?,
      claims: claims == freezed
          ? _value.claims
          : claims // ignore: cast_nullable_to_non_nullable
              as List<String>?,
      clubId: clubId == freezed
          ? _value.clubId
          : clubId // ignore: cast_nullable_to_non_nullable
              as String?,
      country: country == freezed
          ? _value.country
          : country // ignore: cast_nullable_to_non_nullable
              as String?,
      favoriteStore: favoriteStore == freezed
          ? _value.favoriteStore
          : favoriteStore // ignore: cast_nullable_to_non_nullable
              as String?,
      favoriteStoreId: favoriteStoreId == freezed
          ? _value.favoriteStoreId
          : favoriteStoreId // ignore: cast_nullable_to_non_nullable
              as int?,
      fullName: fullName == freezed
          ? _value.fullName
          : fullName // ignore: cast_nullable_to_non_nullable
              as String?,
      gender: gender == freezed
          ? _value.gender
          : gender // ignore: cast_nullable_to_non_nullable
              as String?,
      houseNumber: houseNumber == freezed
          ? _value.houseNumber
          : houseNumber // ignore: cast_nullable_to_non_nullable
              as String?,
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String?,
      landlinePhone: landlinePhone == freezed
          ? _value.landlinePhone
          : landlinePhone // ignore: cast_nullable_to_non_nullable
              as String?,
      language: language == freezed
          ? _value.language
          : language // ignore: cast_nullable_to_non_nullable
              as String?,
      phoneNumber: phoneNumber == freezed
          ? _value.phoneNumber
          : phoneNumber // ignore: cast_nullable_to_non_nullable
              as String?,
      roles: roles == freezed
          ? _value.roles
          : roles // ignore: cast_nullable_to_non_nullable
              as List<String>?,
      street: street == freezed
          ? _value.street
          : street // ignore: cast_nullable_to_non_nullable
              as String?,
      url: url == freezed
          ? _value.url
          : url // ignore: cast_nullable_to_non_nullable
              as String?,
      userName: userName == freezed
          ? _value.userName
          : userName // ignore: cast_nullable_to_non_nullable
              as String?,
      zipCode: zipCode == freezed
          ? _value.zipCode
          : zipCode // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
abstract class _$ProfileCopyWith<$Res> implements $ProfileCopyWith<$Res> {
  factory _$ProfileCopyWith(_Profile value, $Res Function(_Profile) then) =
      __$ProfileCopyWithImpl<$Res>;
  @override
  $Res call(
      {bool disablePushNotifications,
      String firstName,
      DateTime joinDate,
      String lastName,
      bool newsLetterSubscription,
      DateTime? birthDate,
      String? city,
      List<String>? claims,
      String? clubId,
      String? country,
      String? favoriteStore,
      int? favoriteStoreId,
      String? fullName,
      String? gender,
      String? houseNumber,
      String? id,
      String? landlinePhone,
      String? language,
      String? phoneNumber,
      List<String>? roles,
      String? street,
      String? url,
      String? userName,
      String? zipCode});
}

/// @nodoc
class __$ProfileCopyWithImpl<$Res> extends _$ProfileCopyWithImpl<$Res>
    implements _$ProfileCopyWith<$Res> {
  __$ProfileCopyWithImpl(_Profile _value, $Res Function(_Profile) _then)
      : super(_value, (v) => _then(v as _Profile));

  @override
  _Profile get _value => super._value as _Profile;

  @override
  $Res call({
    Object? disablePushNotifications = freezed,
    Object? firstName = freezed,
    Object? joinDate = freezed,
    Object? lastName = freezed,
    Object? newsLetterSubscription = freezed,
    Object? birthDate = freezed,
    Object? city = freezed,
    Object? claims = freezed,
    Object? clubId = freezed,
    Object? country = freezed,
    Object? favoriteStore = freezed,
    Object? favoriteStoreId = freezed,
    Object? fullName = freezed,
    Object? gender = freezed,
    Object? houseNumber = freezed,
    Object? id = freezed,
    Object? landlinePhone = freezed,
    Object? language = freezed,
    Object? phoneNumber = freezed,
    Object? roles = freezed,
    Object? street = freezed,
    Object? url = freezed,
    Object? userName = freezed,
    Object? zipCode = freezed,
  }) {
    return _then(_Profile(
      disablePushNotifications: disablePushNotifications == freezed
          ? _value.disablePushNotifications
          : disablePushNotifications // ignore: cast_nullable_to_non_nullable
              as bool,
      firstName: firstName == freezed
          ? _value.firstName
          : firstName // ignore: cast_nullable_to_non_nullable
              as String,
      joinDate: joinDate == freezed
          ? _value.joinDate
          : joinDate // ignore: cast_nullable_to_non_nullable
              as DateTime,
      lastName: lastName == freezed
          ? _value.lastName
          : lastName // ignore: cast_nullable_to_non_nullable
              as String,
      newsLetterSubscription: newsLetterSubscription == freezed
          ? _value.newsLetterSubscription
          : newsLetterSubscription // ignore: cast_nullable_to_non_nullable
              as bool,
      birthDate: birthDate == freezed
          ? _value.birthDate
          : birthDate // ignore: cast_nullable_to_non_nullable
              as DateTime?,
      city: city == freezed
          ? _value.city
          : city // ignore: cast_nullable_to_non_nullable
              as String?,
      claims: claims == freezed
          ? _value.claims
          : claims // ignore: cast_nullable_to_non_nullable
              as List<String>?,
      clubId: clubId == freezed
          ? _value.clubId
          : clubId // ignore: cast_nullable_to_non_nullable
              as String?,
      country: country == freezed
          ? _value.country
          : country // ignore: cast_nullable_to_non_nullable
              as String?,
      favoriteStore: favoriteStore == freezed
          ? _value.favoriteStore
          : favoriteStore // ignore: cast_nullable_to_non_nullable
              as String?,
      favoriteStoreId: favoriteStoreId == freezed
          ? _value.favoriteStoreId
          : favoriteStoreId // ignore: cast_nullable_to_non_nullable
              as int?,
      fullName: fullName == freezed
          ? _value.fullName
          : fullName // ignore: cast_nullable_to_non_nullable
              as String?,
      gender: gender == freezed
          ? _value.gender
          : gender // ignore: cast_nullable_to_non_nullable
              as String?,
      houseNumber: houseNumber == freezed
          ? _value.houseNumber
          : houseNumber // ignore: cast_nullable_to_non_nullable
              as String?,
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String?,
      landlinePhone: landlinePhone == freezed
          ? _value.landlinePhone
          : landlinePhone // ignore: cast_nullable_to_non_nullable
              as String?,
      language: language == freezed
          ? _value.language
          : language // ignore: cast_nullable_to_non_nullable
              as String?,
      phoneNumber: phoneNumber == freezed
          ? _value.phoneNumber
          : phoneNumber // ignore: cast_nullable_to_non_nullable
              as String?,
      roles: roles == freezed
          ? _value.roles
          : roles // ignore: cast_nullable_to_non_nullable
              as List<String>?,
      street: street == freezed
          ? _value.street
          : street // ignore: cast_nullable_to_non_nullable
              as String?,
      url: url == freezed
          ? _value.url
          : url // ignore: cast_nullable_to_non_nullable
              as String?,
      userName: userName == freezed
          ? _value.userName
          : userName // ignore: cast_nullable_to_non_nullable
              as String?,
      zipCode: zipCode == freezed
          ? _value.zipCode
          : zipCode // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_Profile implements _Profile {
  const _$_Profile(
      {required this.disablePushNotifications,
      required this.firstName,
      required this.joinDate,
      required this.lastName,
      required this.newsLetterSubscription,
      this.birthDate,
      this.city,
      this.claims,
      this.clubId,
      this.country,
      this.favoriteStore,
      this.favoriteStoreId,
      this.fullName,
      this.gender,
      this.houseNumber,
      this.id,
      this.landlinePhone,
      this.language,
      this.phoneNumber,
      this.roles,
      this.street,
      this.url,
      this.userName,
      this.zipCode});

  factory _$_Profile.fromJson(Map<String, dynamic> json) =>
      _$$_ProfileFromJson(json);

  @override
  final bool disablePushNotifications;
  @override
  final String firstName;
  @override
  final DateTime joinDate;
  @override
  final String lastName;
  @override
  final bool newsLetterSubscription;
  @override
  final DateTime? birthDate;
  @override
  final String? city;
  @override
  final List<String>? claims;
  @override
  final String? clubId;
  @override
  final String? country;
  @override
  final String? favoriteStore;
  @override
  final int? favoriteStoreId;
  @override
  final String? fullName;
  @override
  final String? gender;
  @override
  final String? houseNumber;
  @override
  final String? id;
  @override
  final String? landlinePhone;
  @override
  final String? language;
  @override
  final String? phoneNumber;
  @override
  final List<String>? roles;
  @override
  final String? street;
  @override
  final String? url;
  @override
  final String? userName;
  @override
  final String? zipCode;

  @override
  String toString() {
    return 'Profile(disablePushNotifications: $disablePushNotifications, firstName: $firstName, joinDate: $joinDate, lastName: $lastName, newsLetterSubscription: $newsLetterSubscription, birthDate: $birthDate, city: $city, claims: $claims, clubId: $clubId, country: $country, favoriteStore: $favoriteStore, favoriteStoreId: $favoriteStoreId, fullName: $fullName, gender: $gender, houseNumber: $houseNumber, id: $id, landlinePhone: $landlinePhone, language: $language, phoneNumber: $phoneNumber, roles: $roles, street: $street, url: $url, userName: $userName, zipCode: $zipCode)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _Profile &&
            const DeepCollectionEquality().equals(
                other.disablePushNotifications, disablePushNotifications) &&
            const DeepCollectionEquality().equals(other.firstName, firstName) &&
            const DeepCollectionEquality().equals(other.joinDate, joinDate) &&
            const DeepCollectionEquality().equals(other.lastName, lastName) &&
            const DeepCollectionEquality()
                .equals(other.newsLetterSubscription, newsLetterSubscription) &&
            const DeepCollectionEquality().equals(other.birthDate, birthDate) &&
            const DeepCollectionEquality().equals(other.city, city) &&
            const DeepCollectionEquality().equals(other.claims, claims) &&
            const DeepCollectionEquality().equals(other.clubId, clubId) &&
            const DeepCollectionEquality().equals(other.country, country) &&
            const DeepCollectionEquality()
                .equals(other.favoriteStore, favoriteStore) &&
            const DeepCollectionEquality()
                .equals(other.favoriteStoreId, favoriteStoreId) &&
            const DeepCollectionEquality().equals(other.fullName, fullName) &&
            const DeepCollectionEquality().equals(other.gender, gender) &&
            const DeepCollectionEquality()
                .equals(other.houseNumber, houseNumber) &&
            const DeepCollectionEquality().equals(other.id, id) &&
            const DeepCollectionEquality()
                .equals(other.landlinePhone, landlinePhone) &&
            const DeepCollectionEquality().equals(other.language, language) &&
            const DeepCollectionEquality()
                .equals(other.phoneNumber, phoneNumber) &&
            const DeepCollectionEquality().equals(other.roles, roles) &&
            const DeepCollectionEquality().equals(other.street, street) &&
            const DeepCollectionEquality().equals(other.url, url) &&
            const DeepCollectionEquality().equals(other.userName, userName) &&
            const DeepCollectionEquality().equals(other.zipCode, zipCode));
  }

  @override
  int get hashCode => Object.hashAll([
        runtimeType,
        const DeepCollectionEquality().hash(disablePushNotifications),
        const DeepCollectionEquality().hash(firstName),
        const DeepCollectionEquality().hash(joinDate),
        const DeepCollectionEquality().hash(lastName),
        const DeepCollectionEquality().hash(newsLetterSubscription),
        const DeepCollectionEquality().hash(birthDate),
        const DeepCollectionEquality().hash(city),
        const DeepCollectionEquality().hash(claims),
        const DeepCollectionEquality().hash(clubId),
        const DeepCollectionEquality().hash(country),
        const DeepCollectionEquality().hash(favoriteStore),
        const DeepCollectionEquality().hash(favoriteStoreId),
        const DeepCollectionEquality().hash(fullName),
        const DeepCollectionEquality().hash(gender),
        const DeepCollectionEquality().hash(houseNumber),
        const DeepCollectionEquality().hash(id),
        const DeepCollectionEquality().hash(landlinePhone),
        const DeepCollectionEquality().hash(language),
        const DeepCollectionEquality().hash(phoneNumber),
        const DeepCollectionEquality().hash(roles),
        const DeepCollectionEquality().hash(street),
        const DeepCollectionEquality().hash(url),
        const DeepCollectionEquality().hash(userName),
        const DeepCollectionEquality().hash(zipCode)
      ]);

  @JsonKey(ignore: true)
  @override
  _$ProfileCopyWith<_Profile> get copyWith =>
      __$ProfileCopyWithImpl<_Profile>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_ProfileToJson(this);
  }
}

abstract class _Profile implements Profile {
  const factory _Profile(
      {required bool disablePushNotifications,
      required String firstName,
      required DateTime joinDate,
      required String lastName,
      required bool newsLetterSubscription,
      DateTime? birthDate,
      String? city,
      List<String>? claims,
      String? clubId,
      String? country,
      String? favoriteStore,
      int? favoriteStoreId,
      String? fullName,
      String? gender,
      String? houseNumber,
      String? id,
      String? landlinePhone,
      String? language,
      String? phoneNumber,
      List<String>? roles,
      String? street,
      String? url,
      String? userName,
      String? zipCode}) = _$_Profile;

  factory _Profile.fromJson(Map<String, dynamic> json) = _$_Profile.fromJson;

  @override
  bool get disablePushNotifications;
  @override
  String get firstName;
  @override
  DateTime get joinDate;
  @override
  String get lastName;
  @override
  bool get newsLetterSubscription;
  @override
  DateTime? get birthDate;
  @override
  String? get city;
  @override
  List<String>? get claims;
  @override
  String? get clubId;
  @override
  String? get country;
  @override
  String? get favoriteStore;
  @override
  int? get favoriteStoreId;
  @override
  String? get fullName;
  @override
  String? get gender;
  @override
  String? get houseNumber;
  @override
  String? get id;
  @override
  String? get landlinePhone;
  @override
  String? get language;
  @override
  String? get phoneNumber;
  @override
  List<String>? get roles;
  @override
  String? get street;
  @override
  String? get url;
  @override
  String? get userName;
  @override
  String? get zipCode;
  @override
  @JsonKey(ignore: true)
  _$ProfileCopyWith<_Profile> get copyWith =>
      throw _privateConstructorUsedError;
}

import 'package:freezed_annotation/freezed_annotation.dart';

part 'profile.freezed.dart';

part 'profile.g.dart';

@freezed
class Profile with _$Profile {
  const factory Profile({
    required bool disablePushNotifications,
    required String firstName,
    required DateTime joinDate,
    required String lastName,
    required bool newsLetterSubscription,
    DateTime? birthDate,
    String? city,
    List<String>? claims,
    String? clubId,
    String? country,
    String? favoriteStore,
    int? favoriteStoreId,
    String? fullName,
    String? gender,
    String? houseNumber,
    String? id,
    String? landlinePhone,
    String? language,
    String? phoneNumber,
    List<String>? roles,
    String? street,
    String? url,
    String? userName,
    String? zipCode,
  }) = _Profile;

  factory Profile.fromJson(Map<String, dynamic> json) =>
      _$ProfileFromJson(json);
}

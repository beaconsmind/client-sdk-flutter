// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'button_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

ButtonModel _$ButtonModelFromJson(Map<String, dynamic> json) {
  return _ButtonModel.fromJson(json);
}

/// @nodoc
class _$ButtonModelTearOff {
  const _$ButtonModelTearOff();

  _ButtonModel call({required String title, required String link}) {
    return _ButtonModel(
      title: title,
      link: link,
    );
  }

  ButtonModel fromJson(Map<String, Object?> json) {
    return ButtonModel.fromJson(json);
  }
}

/// @nodoc
const $ButtonModel = _$ButtonModelTearOff();

/// @nodoc
mixin _$ButtonModel {
  String get title => throw _privateConstructorUsedError;
  String get link => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $ButtonModelCopyWith<ButtonModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ButtonModelCopyWith<$Res> {
  factory $ButtonModelCopyWith(
          ButtonModel value, $Res Function(ButtonModel) then) =
      _$ButtonModelCopyWithImpl<$Res>;
  $Res call({String title, String link});
}

/// @nodoc
class _$ButtonModelCopyWithImpl<$Res> implements $ButtonModelCopyWith<$Res> {
  _$ButtonModelCopyWithImpl(this._value, this._then);

  final ButtonModel _value;
  // ignore: unused_field
  final $Res Function(ButtonModel) _then;

  @override
  $Res call({
    Object? title = freezed,
    Object? link = freezed,
  }) {
    return _then(_value.copyWith(
      title: title == freezed
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as String,
      link: link == freezed
          ? _value.link
          : link // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
abstract class _$ButtonModelCopyWith<$Res>
    implements $ButtonModelCopyWith<$Res> {
  factory _$ButtonModelCopyWith(
          _ButtonModel value, $Res Function(_ButtonModel) then) =
      __$ButtonModelCopyWithImpl<$Res>;
  @override
  $Res call({String title, String link});
}

/// @nodoc
class __$ButtonModelCopyWithImpl<$Res> extends _$ButtonModelCopyWithImpl<$Res>
    implements _$ButtonModelCopyWith<$Res> {
  __$ButtonModelCopyWithImpl(
      _ButtonModel _value, $Res Function(_ButtonModel) _then)
      : super(_value, (v) => _then(v as _ButtonModel));

  @override
  _ButtonModel get _value => super._value as _ButtonModel;

  @override
  $Res call({
    Object? title = freezed,
    Object? link = freezed,
  }) {
    return _then(_ButtonModel(
      title: title == freezed
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as String,
      link: link == freezed
          ? _value.link
          : link // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_ButtonModel implements _ButtonModel {
  const _$_ButtonModel({required this.title, required this.link});

  factory _$_ButtonModel.fromJson(Map<String, dynamic> json) =>
      _$$_ButtonModelFromJson(json);

  @override
  final String title;
  @override
  final String link;

  @override
  String toString() {
    return 'ButtonModel(title: $title, link: $link)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _ButtonModel &&
            const DeepCollectionEquality().equals(other.title, title) &&
            const DeepCollectionEquality().equals(other.link, link));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(title),
      const DeepCollectionEquality().hash(link));

  @JsonKey(ignore: true)
  @override
  _$ButtonModelCopyWith<_ButtonModel> get copyWith =>
      __$ButtonModelCopyWithImpl<_ButtonModel>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_ButtonModelToJson(this);
  }
}

abstract class _ButtonModel implements ButtonModel {
  const factory _ButtonModel({required String title, required String link}) =
      _$_ButtonModel;

  factory _ButtonModel.fromJson(Map<String, dynamic> json) =
      _$_ButtonModel.fromJson;

  @override
  String get title;
  @override
  String get link;
  @override
  @JsonKey(ignore: true)
  _$ButtonModelCopyWith<_ButtonModel> get copyWith =>
      throw _privateConstructorUsedError;
}

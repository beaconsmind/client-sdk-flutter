// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'beacon_contacts_summary.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

BeaconContactsSummary _$BeaconContactsSummaryFromJson(
    Map<String, dynamic> json) {
  return _BeaconContactsSummary.fromJson(json);
}

/// @nodoc
class _$BeaconContactsSummaryTearOff {
  const _$BeaconContactsSummaryTearOff();

  _BeaconContactsSummary call(
      {required String uuid,
      required String major,
      required String minor,
      required String name,
      required String store,
      required double? currentRSSI,
      required double? averageRSSI,
      required int? lastContactTimestamp,
      required int contactFrequencyPerMinute,
      required int contactsCount,
      required bool isInRange}) {
    return _BeaconContactsSummary(
      uuid: uuid,
      major: major,
      minor: minor,
      name: name,
      store: store,
      currentRSSI: currentRSSI,
      averageRSSI: averageRSSI,
      lastContactTimestamp: lastContactTimestamp,
      contactFrequencyPerMinute: contactFrequencyPerMinute,
      contactsCount: contactsCount,
      isInRange: isInRange,
    );
  }

  BeaconContactsSummary fromJson(Map<String, Object?> json) {
    return BeaconContactsSummary.fromJson(json);
  }
}

/// @nodoc
const $BeaconContactsSummary = _$BeaconContactsSummaryTearOff();

/// @nodoc
mixin _$BeaconContactsSummary {
  /// Beacon's uuid.
  String get uuid => throw _privateConstructorUsedError;

  /// Beacon's major.
  String get major => throw _privateConstructorUsedError;

  /// Beacon's minor.
  String get minor => throw _privateConstructorUsedError;

  /// Beacon's name
  String get name => throw _privateConstructorUsedError;

  /// The name of the store that the beacon is located in.
  String get store => throw _privateConstructorUsedError;

  /// Beacon's current signal strength in dB. Null if the beacon has not been
  /// contacted yet.
  ///
  /// RSSI is an abbreviation from Received Signal Strength Indicator.
  ///
  /// See:
  /// * https://en.wikipedia.org/wiki/Received_signal_strength_indication
  double? get currentRSSI => throw _privateConstructorUsedError;

  /// Beacon's average signal strength in dB (based on the last contacts).
  /// Null if the beacon has not been contacted yet.
  double? get averageRSSI => throw _privateConstructorUsedError;

  /// Last beacon contact timestamp represented as
  /// unix timestamp in milliseconds.
  ///
  /// Null when the beacon has not been contacted yet.
  int? get lastContactTimestamp => throw _privateConstructorUsedError;

  /// Contacts frequency per minute.
  int get contactFrequencyPerMinute => throw _privateConstructorUsedError;

  /// Indicates how many times the beacon was contacted.
  int get contactsCount => throw _privateConstructorUsedError;

  /// Returns if the beacon is within range.
  bool get isInRange => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $BeaconContactsSummaryCopyWith<BeaconContactsSummary> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $BeaconContactsSummaryCopyWith<$Res> {
  factory $BeaconContactsSummaryCopyWith(BeaconContactsSummary value,
          $Res Function(BeaconContactsSummary) then) =
      _$BeaconContactsSummaryCopyWithImpl<$Res>;
  $Res call(
      {String uuid,
      String major,
      String minor,
      String name,
      String store,
      double? currentRSSI,
      double? averageRSSI,
      int? lastContactTimestamp,
      int contactFrequencyPerMinute,
      int contactsCount,
      bool isInRange});
}

/// @nodoc
class _$BeaconContactsSummaryCopyWithImpl<$Res>
    implements $BeaconContactsSummaryCopyWith<$Res> {
  _$BeaconContactsSummaryCopyWithImpl(this._value, this._then);

  final BeaconContactsSummary _value;
  // ignore: unused_field
  final $Res Function(BeaconContactsSummary) _then;

  @override
  $Res call({
    Object? uuid = freezed,
    Object? major = freezed,
    Object? minor = freezed,
    Object? name = freezed,
    Object? store = freezed,
    Object? currentRSSI = freezed,
    Object? averageRSSI = freezed,
    Object? lastContactTimestamp = freezed,
    Object? contactFrequencyPerMinute = freezed,
    Object? contactsCount = freezed,
    Object? isInRange = freezed,
  }) {
    return _then(_value.copyWith(
      uuid: uuid == freezed
          ? _value.uuid
          : uuid // ignore: cast_nullable_to_non_nullable
              as String,
      major: major == freezed
          ? _value.major
          : major // ignore: cast_nullable_to_non_nullable
              as String,
      minor: minor == freezed
          ? _value.minor
          : minor // ignore: cast_nullable_to_non_nullable
              as String,
      name: name == freezed
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      store: store == freezed
          ? _value.store
          : store // ignore: cast_nullable_to_non_nullable
              as String,
      currentRSSI: currentRSSI == freezed
          ? _value.currentRSSI
          : currentRSSI // ignore: cast_nullable_to_non_nullable
              as double?,
      averageRSSI: averageRSSI == freezed
          ? _value.averageRSSI
          : averageRSSI // ignore: cast_nullable_to_non_nullable
              as double?,
      lastContactTimestamp: lastContactTimestamp == freezed
          ? _value.lastContactTimestamp
          : lastContactTimestamp // ignore: cast_nullable_to_non_nullable
              as int?,
      contactFrequencyPerMinute: contactFrequencyPerMinute == freezed
          ? _value.contactFrequencyPerMinute
          : contactFrequencyPerMinute // ignore: cast_nullable_to_non_nullable
              as int,
      contactsCount: contactsCount == freezed
          ? _value.contactsCount
          : contactsCount // ignore: cast_nullable_to_non_nullable
              as int,
      isInRange: isInRange == freezed
          ? _value.isInRange
          : isInRange // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc
abstract class _$BeaconContactsSummaryCopyWith<$Res>
    implements $BeaconContactsSummaryCopyWith<$Res> {
  factory _$BeaconContactsSummaryCopyWith(_BeaconContactsSummary value,
          $Res Function(_BeaconContactsSummary) then) =
      __$BeaconContactsSummaryCopyWithImpl<$Res>;
  @override
  $Res call(
      {String uuid,
      String major,
      String minor,
      String name,
      String store,
      double? currentRSSI,
      double? averageRSSI,
      int? lastContactTimestamp,
      int contactFrequencyPerMinute,
      int contactsCount,
      bool isInRange});
}

/// @nodoc
class __$BeaconContactsSummaryCopyWithImpl<$Res>
    extends _$BeaconContactsSummaryCopyWithImpl<$Res>
    implements _$BeaconContactsSummaryCopyWith<$Res> {
  __$BeaconContactsSummaryCopyWithImpl(_BeaconContactsSummary _value,
      $Res Function(_BeaconContactsSummary) _then)
      : super(_value, (v) => _then(v as _BeaconContactsSummary));

  @override
  _BeaconContactsSummary get _value => super._value as _BeaconContactsSummary;

  @override
  $Res call({
    Object? uuid = freezed,
    Object? major = freezed,
    Object? minor = freezed,
    Object? name = freezed,
    Object? store = freezed,
    Object? currentRSSI = freezed,
    Object? averageRSSI = freezed,
    Object? lastContactTimestamp = freezed,
    Object? contactFrequencyPerMinute = freezed,
    Object? contactsCount = freezed,
    Object? isInRange = freezed,
  }) {
    return _then(_BeaconContactsSummary(
      uuid: uuid == freezed
          ? _value.uuid
          : uuid // ignore: cast_nullable_to_non_nullable
              as String,
      major: major == freezed
          ? _value.major
          : major // ignore: cast_nullable_to_non_nullable
              as String,
      minor: minor == freezed
          ? _value.minor
          : minor // ignore: cast_nullable_to_non_nullable
              as String,
      name: name == freezed
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      store: store == freezed
          ? _value.store
          : store // ignore: cast_nullable_to_non_nullable
              as String,
      currentRSSI: currentRSSI == freezed
          ? _value.currentRSSI
          : currentRSSI // ignore: cast_nullable_to_non_nullable
              as double?,
      averageRSSI: averageRSSI == freezed
          ? _value.averageRSSI
          : averageRSSI // ignore: cast_nullable_to_non_nullable
              as double?,
      lastContactTimestamp: lastContactTimestamp == freezed
          ? _value.lastContactTimestamp
          : lastContactTimestamp // ignore: cast_nullable_to_non_nullable
              as int?,
      contactFrequencyPerMinute: contactFrequencyPerMinute == freezed
          ? _value.contactFrequencyPerMinute
          : contactFrequencyPerMinute // ignore: cast_nullable_to_non_nullable
              as int,
      contactsCount: contactsCount == freezed
          ? _value.contactsCount
          : contactsCount // ignore: cast_nullable_to_non_nullable
              as int,
      isInRange: isInRange == freezed
          ? _value.isInRange
          : isInRange // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_BeaconContactsSummary implements _BeaconContactsSummary {
  _$_BeaconContactsSummary(
      {required this.uuid,
      required this.major,
      required this.minor,
      required this.name,
      required this.store,
      required this.currentRSSI,
      required this.averageRSSI,
      required this.lastContactTimestamp,
      required this.contactFrequencyPerMinute,
      required this.contactsCount,
      required this.isInRange});

  factory _$_BeaconContactsSummary.fromJson(Map<String, dynamic> json) =>
      _$$_BeaconContactsSummaryFromJson(json);

  @override

  /// Beacon's uuid.
  final String uuid;
  @override

  /// Beacon's major.
  final String major;
  @override

  /// Beacon's minor.
  final String minor;
  @override

  /// Beacon's name
  final String name;
  @override

  /// The name of the store that the beacon is located in.
  final String store;
  @override

  /// Beacon's current signal strength in dB. Null if the beacon has not been
  /// contacted yet.
  ///
  /// RSSI is an abbreviation from Received Signal Strength Indicator.
  ///
  /// See:
  /// * https://en.wikipedia.org/wiki/Received_signal_strength_indication
  final double? currentRSSI;
  @override

  /// Beacon's average signal strength in dB (based on the last contacts).
  /// Null if the beacon has not been contacted yet.
  final double? averageRSSI;
  @override

  /// Last beacon contact timestamp represented as
  /// unix timestamp in milliseconds.
  ///
  /// Null when the beacon has not been contacted yet.
  final int? lastContactTimestamp;
  @override

  /// Contacts frequency per minute.
  final int contactFrequencyPerMinute;
  @override

  /// Indicates how many times the beacon was contacted.
  final int contactsCount;
  @override

  /// Returns if the beacon is within range.
  final bool isInRange;

  @override
  String toString() {
    return 'BeaconContactsSummary(uuid: $uuid, major: $major, minor: $minor, name: $name, store: $store, currentRSSI: $currentRSSI, averageRSSI: $averageRSSI, lastContactTimestamp: $lastContactTimestamp, contactFrequencyPerMinute: $contactFrequencyPerMinute, contactsCount: $contactsCount, isInRange: $isInRange)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _BeaconContactsSummary &&
            const DeepCollectionEquality().equals(other.uuid, uuid) &&
            const DeepCollectionEquality().equals(other.major, major) &&
            const DeepCollectionEquality().equals(other.minor, minor) &&
            const DeepCollectionEquality().equals(other.name, name) &&
            const DeepCollectionEquality().equals(other.store, store) &&
            const DeepCollectionEquality()
                .equals(other.currentRSSI, currentRSSI) &&
            const DeepCollectionEquality()
                .equals(other.averageRSSI, averageRSSI) &&
            const DeepCollectionEquality()
                .equals(other.lastContactTimestamp, lastContactTimestamp) &&
            const DeepCollectionEquality().equals(
                other.contactFrequencyPerMinute, contactFrequencyPerMinute) &&
            const DeepCollectionEquality()
                .equals(other.contactsCount, contactsCount) &&
            const DeepCollectionEquality().equals(other.isInRange, isInRange));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(uuid),
      const DeepCollectionEquality().hash(major),
      const DeepCollectionEquality().hash(minor),
      const DeepCollectionEquality().hash(name),
      const DeepCollectionEquality().hash(store),
      const DeepCollectionEquality().hash(currentRSSI),
      const DeepCollectionEquality().hash(averageRSSI),
      const DeepCollectionEquality().hash(lastContactTimestamp),
      const DeepCollectionEquality().hash(contactFrequencyPerMinute),
      const DeepCollectionEquality().hash(contactsCount),
      const DeepCollectionEquality().hash(isInRange));

  @JsonKey(ignore: true)
  @override
  _$BeaconContactsSummaryCopyWith<_BeaconContactsSummary> get copyWith =>
      __$BeaconContactsSummaryCopyWithImpl<_BeaconContactsSummary>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_BeaconContactsSummaryToJson(this);
  }
}

abstract class _BeaconContactsSummary implements BeaconContactsSummary {
  factory _BeaconContactsSummary(
      {required String uuid,
      required String major,
      required String minor,
      required String name,
      required String store,
      required double? currentRSSI,
      required double? averageRSSI,
      required int? lastContactTimestamp,
      required int contactFrequencyPerMinute,
      required int contactsCount,
      required bool isInRange}) = _$_BeaconContactsSummary;

  factory _BeaconContactsSummary.fromJson(Map<String, dynamic> json) =
      _$_BeaconContactsSummary.fromJson;

  @override

  /// Beacon's uuid.
  String get uuid;
  @override

  /// Beacon's major.
  String get major;
  @override

  /// Beacon's minor.
  String get minor;
  @override

  /// Beacon's name
  String get name;
  @override

  /// The name of the store that the beacon is located in.
  String get store;
  @override

  /// Beacon's current signal strength in dB. Null if the beacon has not been
  /// contacted yet.
  ///
  /// RSSI is an abbreviation from Received Signal Strength Indicator.
  ///
  /// See:
  /// * https://en.wikipedia.org/wiki/Received_signal_strength_indication
  double? get currentRSSI;
  @override

  /// Beacon's average signal strength in dB (based on the last contacts).
  /// Null if the beacon has not been contacted yet.
  double? get averageRSSI;
  @override

  /// Last beacon contact timestamp represented as
  /// unix timestamp in milliseconds.
  ///
  /// Null when the beacon has not been contacted yet.
  int? get lastContactTimestamp;
  @override

  /// Contacts frequency per minute.
  int get contactFrequencyPerMinute;
  @override

  /// Indicates how many times the beacon was contacted.
  int get contactsCount;
  @override

  /// Returns if the beacon is within range.
  bool get isInRange;
  @override
  @JsonKey(ignore: true)
  _$BeaconContactsSummaryCopyWith<_BeaconContactsSummary> get copyWith =>
      throw _privateConstructorUsedError;
}

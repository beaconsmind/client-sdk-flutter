import 'package:freezed_annotation/freezed_annotation.dart';

part 'beacon_contacts_summary.freezed.dart';
part 'beacon_contacts_summary.g.dart';

/// {@template beacon_contacts_summary}
/// A class that holds information about the beacon device.
///
/// It consists of the beacon unique identifier ([uuid], [minor], [major]),
/// the beacon's name ([name]) and the name of the [store] that it is located in.
///
/// Additionally, it contains data related to its signal strength and the number
/// of times it was contacted.
/// {@endtemplate}
@freezed
class BeaconContactsSummary with _$BeaconContactsSummary {
  /// {@macro beacon_contacts_summary}
  factory BeaconContactsSummary({
    /// Beacon's uuid.
    required String uuid,

    /// Beacon's major.
    required String major,

    /// Beacon's minor.
    required String minor,

    /// Beacon's name
    required String name,

    /// The name of the store that the beacon is located in.
    required String store,

    /// Beacon's current signal strength in dB. Null if the beacon has not been
    /// contacted yet.
    ///
    /// RSSI is an abbreviation from Received Signal Strength Indicator.
    ///
    /// See:
    /// * https://en.wikipedia.org/wiki/Received_signal_strength_indication
    required double? currentRSSI,

    /// Beacon's average signal strength in dB (based on the last contacts).
    /// Null if the beacon has not been contacted yet.
    required double? averageRSSI,

    /// Last beacon contact timestamp represented as
    /// unix timestamp in milliseconds.
    ///
    /// Null when the beacon has not been contacted yet.
    required int? lastContactTimestamp,

    /// Contacts frequency per minute.
    required int contactFrequencyPerMinute,

    /// Indicates how many times the beacon was contacted.
    required int contactsCount,

    /// Returns if the beacon is within range.
    required bool isInRange,
  }) = _BeaconContactsSummary;

  /// Creates a new instance of [BeaconContactsSummary] from a json map.
  factory BeaconContactsSummary.fromJson(Map<String, dynamic> json) =>
      _$BeaconContactsSummaryFromJson(json);
}

// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'beacon_contacts_summary.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_BeaconContactsSummary _$$_BeaconContactsSummaryFromJson(
        Map<String, dynamic> json) =>
    _$_BeaconContactsSummary(
      uuid: json['uuid'] as String,
      major: json['major'] as String,
      minor: json['minor'] as String,
      name: json['name'] as String,
      store: json['store'] as String,
      currentRSSI: (json['currentRSSI'] as num?)?.toDouble(),
      averageRSSI: (json['averageRSSI'] as num?)?.toDouble(),
      lastContactTimestamp: json['lastContactTimestamp'] as int?,
      contactFrequencyPerMinute: json['contactFrequencyPerMinute'] as int,
      contactsCount: json['contactsCount'] as int,
      isInRange: json['isInRange'] as bool,
    );

Map<String, dynamic> _$$_BeaconContactsSummaryToJson(
        _$_BeaconContactsSummary instance) =>
    <String, dynamic>{
      'uuid': instance.uuid,
      'major': instance.major,
      'minor': instance.minor,
      'name': instance.name,
      'store': instance.store,
      'currentRSSI': instance.currentRSSI,
      'averageRSSI': instance.averageRSSI,
      'lastContactTimestamp': instance.lastContactTimestamp,
      'contactFrequencyPerMinute': instance.contactFrequencyPerMinute,
      'contactsCount': instance.contactsCount,
      'isInRange': instance.isInRange,
    };

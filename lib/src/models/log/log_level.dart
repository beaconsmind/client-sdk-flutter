enum LogLevel {
  /// Any developer-level debug info, repeating events, e.g.:
  /// - monitoring/location callbacks,
  /// - networking success results.
  debug,

  /// Bigger events/steps in the SDK lifecycle, e.g.:
  /// - starting/stopping services,
  /// - initializing SDK,
  /// - one-time events.
  info,

  /// Allowed, but not-optimal, inconsistent state, e.g.:
  /// - trying to start monitoring without permissions.
  warning,

  /// Not allowed state (any error).
  error,

  /// No logging at all.
  silent,
}

extension LogLevelExtension on LogLevel {
  String parseToString() {
    switch (this) {
      case LogLevel.debug:
        return 'debug';
      case LogLevel.info:
        return 'info';
      case LogLevel.warning:
        return 'warning';
      case LogLevel.error:
        return 'error';
      case LogLevel.silent:
        return 'silent';
    }
  }
}

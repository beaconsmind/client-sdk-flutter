/// Contains start options specific to Android platform.
class AndroidStartOptions {
  /// Constructs an instance of [AndroidStartOptions].
  const AndroidStartOptions({
    this.notificationOptions,
    this.usePassiveScanning = true,
  });

  /// Options that describe the content of the native Android notification that
  /// is displayed to user when a foreground service responsible for scanning
  /// beacons is running.
  ///
  /// The notification is displayed when [usePassiveScanning] is set to `false`.
  final AndroidNotificationOptions? notificationOptions;

  /// The SDK supports two types of BLE scanning:
  /// - active scanning,
  /// - passive scanning.
  ///
  /// When scanning actively, the Beaconsmind SDK will start a foreground
  /// service and show a pinned notification described by [notificationOptions].
  ///
  /// By default, the SDK uses passive scanning, i.e. [usePassiveScanning] is
  /// set to true.
  final bool usePassiveScanning;
}

class AndroidNotificationOptions {
  const AndroidNotificationOptions({
    this.badgeName,
    this.title,
    this.text,
    this.channelName,
  });

  /// Name of the native icon resource.
  ///
  /// Default value: "ic_beacons".
  ///
  /// Reference:
  /// * https://developer.android.com/reference/androidx/core/app/NotificationCompat.Builder#setSmallIcon(int)
  final String? badgeName;

  /// Title of the notification (first row).
  ///
  /// Default value: "Beaconsmind".
  ///
  /// Reference:
  /// * https://developer.android.com/reference/androidx/core/app/NotificationCompat.Builder#setContentTitle(java.lang.CharSequence)
  final String? title;

  /// Body of the notification (second row).
  ///
  /// Default value: "Listening for Beacons".
  ///
  /// Reference:
  /// * https://developer.android.com/reference/androidx/core/app/NotificationCompat.Builder#setContentText(java.lang.CharSequence)
  final String? text;

  /// The user visible name of the channel.
  ///
  /// Default value: "beaconsmind".
  ///
  /// Reference:
  /// * https://developer.android.com/reference/android/app/NotificationChannel#NotificationChannel(java.lang.String,%20java.lang.CharSequence,%20int)
  final String? channelName;
}

/// Contains start options specific to iOS platform.
///
/// Currently there are no options specific to iOS platform when starting the
/// beaconsmind SDK.
class IOSStartOptions {
  /// Constructs an instance of [IOSStartOptions].
  const IOSStartOptions();
}

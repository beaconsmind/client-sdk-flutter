import 'platform_specifics/platform_specifics.dart';

export 'platform_specifics/platform_specifics.dart';

/// Contains start options specific to each platform.
class StartOptions {
  /// Constructs an instance of [StartOptions].
  const StartOptions({
    this.android,
    this.iOS,
  });

  /// Notification details for Android.
  final AndroidStartOptions? android;

  /// Notification details for iOS.
  final IOSStartOptions? iOS;
}

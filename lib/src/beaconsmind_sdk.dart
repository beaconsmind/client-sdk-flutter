import 'dart:async';
import 'dart:convert';

import 'package:beaconsmind_sdk/src/models/beacon_contacts_summary/beacon_contacts_summary.dart';
import 'package:beaconsmind_sdk/src/models/log/log.dart';
import 'package:beaconsmind_sdk/src/models/start_options/start_options.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';
import 'package:beaconsmind_sdk/src/models/beacon/beacon.dart';
import 'package:beaconsmind_sdk/src/models/offer/offer.dart';
import 'package:beaconsmind_sdk/src/models/profile/profile.dart';
import 'package:beaconsmind_sdk/src/sdk_context.dart';
import 'package:rxdart/rxdart.dart';

class Beaconsmind {
  static const MethodChannel _channel = MethodChannel('beaconsmind/methods');
  static const EventChannel _contextEvents =
      EventChannel('beaconsmind/contextEvents');

  Beaconsmind();

  static Beaconsmind? _instance;

  static Beaconsmind get instance {
    _instance ??= Beaconsmind();
    return _instance!;
  }

  BehaviorSubject<BeaconsmindSdkContext?>? _contextEventsController;

  ///  Initializes the SDK.
  ///
  ///  Call this method as soon as your app starts, preferably in your [main.dart]
  ///  so the sdk can track all touch points and beacon events correctly.
  Future<void> initialize({
    /// App version.
    required String appVersion,

    /// Host name, example: https://adidas.bms.beaconsmind.com
    required String hostname,

    /// Platform specific options.
    ///
    /// See:
    /// * [AndroidStartOptions]
    /// * [IOSStartOptions]
    StartOptions? options,
  }) async {
    if (defaultTargetPlatform == TargetPlatform.android) {
      final notificationOptions = options?.android?.notificationOptions;
      await _channel.invokeMethod('initialize', {
        'appVersion': appVersion,
        'hostname': hostname,
        'usePassiveScanning': options?.android?.usePassiveScanning,
        'notificationBadgeName': notificationOptions?.badgeName,
        'notificationTitle': notificationOptions?.title,
        'notificationText': notificationOptions?.text,
        'notificationChannelName': notificationOptions?.channelName,
      });
    } else if (defaultTargetPlatform == TargetPlatform.iOS) {
      await _channel.invokeMethod('initialize', {
        'appVersion': appVersion,
        'hostname': hostname,
      });
    } else {
      throw UnsupportedError(
          'Beaconsmind SDK is not supported on $defaultTargetPlatform platform.');
    }
  }

  /// **For development purposes only.**
  ///
  /// Alternative initialization method.
  ///
  /// Kickstart sdk testing and integration by a single method call. Invokes
  /// [initialize] and sets the sdk in development mode.
  ///
  /// When initialized with this method the sdk requests permissions on it own.
  ///
  /// In order to use it, make sure to follow the platform-specific steps listed
  /// below.
  ///
  /// ## Android
  ///
  /// It will request location & bluetooth permissions that are
  /// required for the Beaconsmind SDK to detect beacon devices.
  ///
  /// In order to use it, make sure to follow those steps:
  ///
  /// 1. Change the MainActivity.kt file to extend FlutterFragmentActivity
  /// instead of FlutterActivity.
  ///
  /// ```kt
  /// import io.flutter.embedding.android.FlutterActivity
  /// import io.flutter.embedding.android.FlutterFragmentActivity
  ///
  /// class MainActivity: FlutterFragmentActivity() {
  /// }
  /// ```
  ///
  /// 3. Use descendant of Theme.AppCompat for your activity theme (both light
  /// and dark). These changes are needed because the Beaconsmind SDK requires
  /// the use of the AppCompat theme for the permission dialog.
  ///
  /// ```xml
  /// <?xml version="1.0" encoding="utf-8"?>
  /// <resources>
  ///     <!-- Theme applied to the Android Window while the process is starting when the OS's Dark Mode setting is off -->
  ///     <style name="LaunchTheme" parent="Theme.AppCompat.Light.NoActionBar">
  ///         <!-- Show a splash screen on the activity. Automatically removed when
  ///              Flutter draws its first frame -->
  ///         <item name="android:windowBackground">@drawable/launch_background</item>
  ///     </style>
  ///     <!-- Theme applied to the Android Window as soon as the process has started.
  ///          This theme determines the color of the Android Window while your
  ///          Flutter UI initializes, as well as behind your Flutter UI while its
  ///          running.
  ///
  ///          This Theme is only used starting with V2 of Flutter's Android embedding. -->
  ///     <style name="NormalTheme" parent="Theme.MaterialComponents">
  ///         <item name="android:windowBackground">?android:colorBackground</item>
  ///     </style>
  /// </resources>
  /// ```
  ///
  /// ## iOS
  ///
  /// On iOS, it will request the location and notification permissions.
  ///
  /// Make sure to add the following key to your Info.plist file, located in <project root>/ios/Runner/Info.plist:
  ///
  /// ```xml
  /// <key>NSLocationAlwaysAndWhenInUseUsageDescription</key>
  /// <string>$(PRODUCT_NAME) needs access to your location to detect beacons.</string>
  /// <key>NSLocationWhenInUseUsageDescription</key>
  /// <string>$(PRODUCT_NAME) needs access to your location to detect beacons.</string>
  /// <key>NSLocationAlwaysUsageDescription</key>
  /// <string>$(PRODUCT_NAME) needs access to your location to detect beacons.</string>
  /// ```
  Future<void> initializeDevelop({
    /// App version.
    required String appVersion,

    /// Host name, example: https://adidas.bms.beaconsmind.com
    required String hostname,

    /// Platform specific options.
    ///
    /// See:
    /// * [AndroidStartOptions]
    /// * [IOSStartOptions]
    StartOptions? options,
  }) async {
    if (defaultTargetPlatform == TargetPlatform.android) {
      final notificationOptions = options?.android?.notificationOptions;
      await _channel.invokeMethod('initializeDevelop', {
        'appVersion': appVersion,
        'hostname': hostname,
        'usePassiveScanning': options?.android?.usePassiveScanning,
        'notificationBadgeName': notificationOptions?.badgeName,
        'notificationTitle': notificationOptions?.title,
        'notificationText': notificationOptions?.text,
        'notificationChannelName': notificationOptions?.channelName,
      });
    } else if (defaultTargetPlatform == TargetPlatform.iOS) {
      await _channel.invokeMethod('initializeDevelop', {
        'appVersion': appVersion,
        'hostname': hostname,
      });
    } else {
      throw UnsupportedError(
          'Beaconsmind SDK is not supported on $defaultTargetPlatform platform.');
    }
  }

  //
  // Start
  //
  @Deprecated("use initialize() instead")
  Future<void> start({
    /// App version.
    required String appVersion,

    /// Host name, example: https://adidas.bms.beaconsmind.com
    required String hostname,

    /// Platform specific options.
    ///
    /// See:
    /// * [AndroidStartOptions]
    /// * [IOSStartOptions]
    StartOptions? options,
  }) async {
    if (defaultTargetPlatform == TargetPlatform.android) {
      final notificationOptions = options?.android?.notificationOptions;
      await _channel.invokeMethod('start', {
        'appVersion': appVersion,
        'hostname': hostname,
        'usePassiveScanning': options?.android?.usePassiveScanning,
        'notificationBadgeName': notificationOptions?.badgeName,
        'notificationTitle': notificationOptions?.title,
        'notificationText': notificationOptions?.text,
        'notificationChannelName': notificationOptions?.channelName,
      });
    } else if (defaultTargetPlatform == TargetPlatform.iOS) {
      await _channel.invokeMethod('start', {
        'appVersion': appVersion,
        'hostname': hostname,
      });
    } else {
      throw UnsupportedError(
          'Beaconsmind SDK is not supported on $defaultTargetPlatform platform.');
    }
  }

  //
  // Authentication
  //
  Future<BeaconsmindSdkContext> signup({
    required String username,
    required String firstName,
    required String lastName,
    required String password,
    required String confirmPassword,
    String? language,
    String? gender,
    int? favoriteStoreID,
    DateTime? birthDate,
    String? countryCode,
  }) async {
    final res = await _channel.invokeMethod<Map>('signup', {
      'username': username,
      'firstName': firstName,
      'lastName': lastName,
      'password': password,
      'confirmPassword': confirmPassword,
      'language': language,
      'gender': gender,
      'favoriteStoreID': favoriteStoreID,
      'birthDateSeconds': birthDate != null
          ? (birthDate.millisecondsSinceEpoch / 1000).toDouble()
          : null,
      'countryCode': countryCode,
    });
    if (res != null) {
      final json = Map<String, dynamic>.from(res);
      return BeaconsmindSdkContext.fromJson(json);
    } else {
      return Future.error('signup failed!');
    }
  }

  Future<BeaconsmindSdkContext> login({
    required String username,
    required String password,
  }) async {
    final res = await _channel.invokeMethod<Map?>('login', {
      'username': username,
      'password': password,
    });
    if (res != null) {
      final json = Map<String, dynamic>.from(res);
      return BeaconsmindSdkContext.fromJson(json);
    } else {
      return Future.error('login failed!');
    }
  }

  Future<BeaconsmindSdkContext> importAccount({
    required String id,
    required String email,
    String? firstName,
    String? lastName,
    DateTime? birthDate,
    String? language,
    String? gender,
  }) async {
    final res = await _channel.invokeMethod<Map?>(
      'importAccount',
      {
        'id': id,
        'email': email,
        'firstName': firstName,
        'lastName': lastName,
        'birthDateSeconds': birthDate != null
            ? (birthDate.millisecondsSinceEpoch / 1000).toDouble()
            : null,
        'language': language,
        'gender': gender,
      },
    );

    final json = Map<String, dynamic>.from(res!);
    return BeaconsmindSdkContext.fromJson(json);
  }

  Future<Profile?> getProfile() async {
    final res = await _channel.invokeMethod<Map?>('getProfile');
    if (res != null) {
      final json = Map<String, dynamic>.from(res);
      return Profile.fromJson(json);
    }
    return null;
  }

  Future<Profile?> updateProfile({
    required String firstName,
    required String lastName,
    DateTime? birthDate,
    String? city,
    String? country,
    bool? disablePushNotifications,
    int? favoriteStoreID,
    String? gender,
    String? houseNumber,
    String? landlinePhone,
    String? language,
    String? phoneNumber,
    String? street,
    String? zipCode,
  }) async {
    final res = await _channel.invokeMethod<Map?>('updateProfile', {
      'firstName': firstName,
      'lastName': lastName,
      'birthDateSeconds': birthDate != null
          ? (birthDate.millisecondsSinceEpoch / 1000).toDouble()
          : null,
      'city': city,
      'country': country,
      'disablePushNotifications': disablePushNotifications,
      'favoriteStoreID': favoriteStoreID,
      'gender': gender,
      'houseNumber': houseNumber,
      'landlinePhone': landlinePhone,
      'language': language,
      'phoneNumber': phoneNumber,
      'street': street,
      'zipCode': zipCode,
    });
    if (res != null) {
      final json = Map<String, dynamic>.from(res);
      return Profile.fromJson(json);
    } else {
      return null;
    }
  }

  //
  // Context helpers
  //
  Future<void> logout() async {
    await _channel.invokeMethod('logout');
  }

  Future<BeaconsmindSdkContext?> getOAuthContext() async {
    final res = await _channel.invokeMethod<Map?>('getAPIContext');

    if (res != null) {
      final json = Map<String, dynamic>.from(res);
      return BeaconsmindSdkContext.fromJson(json);
    } else {
      return null;
    }
  }

  Future<void> updateHostname({
    required String hostname,
  }) async {
    await _channel.invokeMethod('updateHostname', {
      'hostname': hostname,
    });
  }

  //
  // Beacons
  //
  Future<void> startListeningBeacons() async {
    await _channel.invokeMethod("startListeningBeacons");
  }

  Future<void> stopListeningBeacons() async {
    await _channel.invokeMethod("stopListeningBeacons");
  }

  @Deprecated("use getBeaconContactsSummary() instead")
  Future<List<Beacon>> getBeaconsSummary() async {
    final res = await await _channel.invokeMethod('getBeaconsSummary');
    final List<dynamic> json = jsonDecode(res);
    return json.map((e) => Beacon.fromJson(e)).toList();
  }

  /// Returns a list of information about the beacons.
  Future<List<BeaconContactsSummary>> getBeaconContactsSummary() async {
    final res = await _channel.invokeMethod('getBeaconContactsSummary') as List;
    return res.map((e) => BeaconContactsSummary.fromJson(Map.from(e))).toList();
  }

  /// Register the push notifications token with Beaconsmind sdk.
  ///
  /// Example usage (FCM):
  /// ```dart
  /// String? fcmToken = await FirebaseMessaging.instance.getToken();
  /// await Beaconsmind.registerDeviceToken(
  ///   token: fcmToken,
  ///   platformType: PlatformType.fcm
  /// );
  /// ```
  ///
  /// Example usage (APNS):
  ///
  /// ```dart
  /// String? apnsToken = await FirebaseMessaging.instance.getAPNSToken();
  /// await Beaconsmind.registerDeviceToken(
  ///   token: apnsToken,
  ///   platformType: PlatformType.apns
  /// );
  /// ```
  Future<void> registerDeviceToken({
    required String deviceToken,
    required PlatformType platformType,
  }) async {
    await _channel.invokeMethod('registerDeviceToken', {
      'deviceToken': deviceToken,
      'platformType': platformType.index,
    });
  }

  //
  // Offers
  //

  /// Return the offerId from push notification data.
  int? parseOfferId({required Map data}) {
    return int.tryParse('${data["offerId"]}');
  }

  Future<void> markOfferAsReceived({required int offerId}) async {
    await _channel.invokeMethod('markOfferAsReceived', {
      'offerId': offerId,
    });
  }

  Future<void> markOfferAsRead({required int offerId}) async {
    await _channel.invokeMethod('markOfferAsRead', {
      'offerId': offerId,
    });
  }

  Future<void> markOfferAsRedeemed({required int offerId}) async {
    await _channel.invokeMethod('markOfferAsRedeemed', {
      'offerId': offerId,
    });
  }

  Future<Offer> loadOffer({required int id}) async {
    final res = await await _channel.invokeMethod('loadOffer', {
      'offerId': id,
    });
    final Map<dynamic, dynamic> json = jsonDecode(res);
    return Offer.fromJson(json.cast<String, dynamic>());
  }

  Future<List<Offer>> loadOffers() async {
    final res = await await _channel.invokeMethod('loadOffers');
    final List<dynamic> json = jsonDecode(res);
    return json.map((e) => Offer.fromJson(e)).toList();
  }

  /// Sets minimum [LogLevel] for the Beaconsmind SDK.
  ///
  /// The [LogLevel] is used to filter the logs that are printed to the console.
  Future<void> setMinLogLevel(LogLevel minLogLevel) async {
    await _channel.invokeMethod('setMinLogLevel', {
      'minLogLevel': minLogLevel.parseToString(),
    });
  }

  //
  // context events stream
  //
  Stream<BeaconsmindSdkContext?> contextEvents() async* {
    if (_contextEventsController == null) {
      _contextEventsController = BehaviorSubject<BeaconsmindSdkContext?>();
      _contextEvents.receiveBroadcastStream().listen((event) {
        if (_contextEventsController?.isClosed == true) {
          return;
        }
        if (event != null) {
          final json = Map<String, dynamic>.from(event);
          try {
            _contextEventsController?.add(BeaconsmindSdkContext.fromJson(json));
          } catch (e, s) {
            _contextEventsController?.addError(e, s);
          }
        } else {
          _contextEventsController?.add(null);
        }
      });
    }
    yield* _contextEventsController!.stream;
  }

  BeaconsmindSdkContext? get currentContext => _contextEventsController?.value;

  /// Call dispose when closing the app, like in a top level dispose function.
  void dispose() {
    if (_contextEventsController?.isClosed != true) {
      _contextEventsController!.close();
    }
    _contextEventsController = null;
    _instance = null;
  }
}

enum PlatformType { unknown, fcm, apns }

# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [1.8.0](https://gitlab.com/beaconsmind/client-sdk-flutter/compare/1.7.0-268...1.8.0) (2023-11-07)


### Features

* add platformType parameter to registerDeviceToken method [5447603697] ([e0dbba9](https://gitlab.com/beaconsmind/client-sdk-flutter/commit/e0dbba9b8b7d3da7fb9de67c7d94221108c289a2))

## [1.7.0](https://gitlab.com/beaconsmind/client-sdk-flutter/compare/1.6.0-257...1.7.0) (2023-05-30)


### Features

* add countryCode parameter to the signUp method ([383c3cd](https://gitlab.com/beaconsmind/client-sdk-flutter/commit/383c3cd1d9b251abbd5f5faaee9be2dbd1b8163e))
* **example:** add country code field to signup screen ([9332e05](https://gitlab.com/beaconsmind/client-sdk-flutter/commit/9332e057b22b98168175960decd518c001ae7235))

## [1.6.0](https://gitlab.com/beaconsmind/client-sdk-flutter/compare/1.5.1...1.6.0) (2023-05-19)


### Features

* implement initializeDevelop method ([8ed7e86](https://gitlab.com/beaconsmind/client-sdk-flutter/commit/8ed7e86fd9b17dc25275085ee63591291fcc4b15))
* upgrade android sdk to 1.12.2 (4392540289) ([bab60b1](https://gitlab.com/beaconsmind/client-sdk-flutter/commit/bab60b19f9f14fef156e4990c317258e4c081f34))
* upgrade ios sdk to 3.8.3 (4392540289) ([3bda55b](https://gitlab.com/beaconsmind/client-sdk-flutter/commit/3bda55b2bf5ed83801399d430fad7fc8892ab931))


### Bug Fixes

* change 'Beaconsmind SDK Flutter Demo' to 'Flutter SDK Demo' for consistency ([ddc31c2](https://gitlab.com/beaconsmind/client-sdk-flutter/commit/ddc31c2f4db2ba05377232a0f9914aa769cdcb69))

### [1.5.1](https://gitlab.com/beaconsmind/client-sdk-flutter/compare/1.5.0...1.5.1) (2022-10-10)

### Features

* upgrade Android SDK to 1.10.1 and iOS SDK to 3.6.6 ([4bfc24](https://gitlab.com/beaconsmind/client-sdk-flutter/-/commit/2de88c7081301cddd7822bc1509e794cce4bfc24))

### Bug Fixes

* **example:** use apns token for ios ([9347d75](https://gitlab.com/beaconsmind/client-sdk-flutter/commit/9347d7593d844b8b1a67f331f0c2eafb8a5700ea))

## [1.5.0](https://gitlab.com/beaconsmind/client-sdk-flutter/compare/1.4.0...1.5.0) (2022-09-12)


### Features

* use beaconsmind sdk initializer ([d081712](https://gitlab.com/beaconsmind/client-sdk-flutter/commit/d081712304d2601a7e7b1ed4883de7a98925369c))


### Bug Fixes

* add tools xmlns ([78eb46f](https://gitlab.com/beaconsmind/client-sdk-flutter/commit/78eb46f4aaa576c3d4efc72c3f5867f802a5b582))
* move provider declaration to plugin ([acab180](https://gitlab.com/beaconsmind/client-sdk-flutter/commit/acab180ff82e235b3e2f642743568c82563380f6))
* update iOS SDK to 3.6.2 ([36991b9](https://gitlab.com/beaconsmind/client-sdk-flutter/commit/36991b9a91c7bd13bc00d25f08a06e8b3c9020e9))
* upgrade client-sdk-android to 1.10.0 ([cce079a](https://gitlab.com/beaconsmind/client-sdk-flutter/commit/cce079ad9e8bcb660a2d160807bdb689b6b7adab))

## [1.4.0](https://gitlab.com/beaconsmind/client-sdk-flutter/compare/1.3.2...1.4.0) (2022-09-07)


### Features

* **example:** allow users to change beaconsmind hostname ([9f7ef09](https://gitlab.com/beaconsmind/client-sdk-flutter/commit/9f7ef099b6b47b9133f00e2a5dc10340989e3fee))


### Bug Fixes

* **example:** add missing bt keys to Info.plist file ([dd3b055](https://gitlab.com/beaconsmind/client-sdk-flutter/commit/dd3b0551bd7e0de73dc6d464dad4d8ec7cf5482a))
* **example:** call setupNotifications in initState ([a985950](https://gitlab.com/beaconsmind/client-sdk-flutter/commit/a9859503234c5c030ca68415c917f39ee48f824d))
* importAccount json deserialization ([f44b2a8](https://gitlab.com/beaconsmind/client-sdk-flutter/commit/f44b2a8afbcaa4a57c5816188672f3408180e119))
* make Offer.callToAction nullable ([2d21908](https://gitlab.com/beaconsmind/client-sdk-flutter/commit/2d219082f428c2a7789022c63e54a823beca96d5))

### [1.3.2](https://gitlab.com/beaconsmind/client-sdk-flutter/compare/1.3.1...1.3.2) (2022-08-02)


### Bug Fixes

* upgrade android sdk to 1.9.3 ([cf46401](https://gitlab.com/beaconsmind/client-sdk-flutter/commit/cf464016c8426e2496d93be27c7b6d114a886a84))

### [1.3.1](https://gitlab.com/beaconsmind/client-sdk-flutter/compare/1.3.0...1.3.1) (2022-07-27)


### Bug Fixes

* set correct content type when sending login request on Android ([3bcfbf5](https://gitlab.com/beaconsmind/client-sdk-flutter/commit/3bcfbf5cec8d6b81bd020f50a8793b4816fc0495))

## [1.3.0](https://gitlab.com/beaconsmind/client-sdk-flutter/compare/1.2.0...1.3.0) (2022-07-26)


### Features

* upgrade Android SDK from 1.9.0 to 1.9.1 ([8fe69eb](https://gitlab.com/beaconsmind/client-sdk-flutter/commit/8fe69eb19832c997cdd9eda2afbed6da71f3aedd))
* upgrade iOS SDK from 3.6.0 to 3.6.1 ([f42e3ba](https://gitlab.com/beaconsmind/client-sdk-flutter/commit/f42e3ba39261779feca1c33e36757dad6c609644))

## [1.2.0](https://gitlab.com/beaconsmind/client-sdk-flutter/compare/1.1.0...1.2.0) (2022-07-22)


### Features

* upgrade Android SDK to 1.9.0 ([ff610a5](https://gitlab.com/beaconsmind/client-sdk-flutter/-/commit/ff610a588d310ac279a64b89b7d9340bb098b4bf))
* add getBeaconContactsSummary method and deprecate getBeaconsSummary ([279afca](https://gitlab.com/beaconsmind/client-sdk-flutter/-/commit/279afca2ea8a4d4dc2dfe9ea323532163109a09a))
* **example:** update beacons screen ([fc26027](https://gitlab.com/beaconsmind/client-sdk-flutter/commit/fc260270167241bceee6d49b4206cca1b151d3d2))

## 1.1.0 (2022-07-15)


### Features

* add setLoggingEnabled method ([f7e6a23](https://gitlab.com/beaconsmind/client-sdk-flutter/commit/f7e6a23676c74d6cf6202506b3be4f05ee850dac))
* **example:** update iOS app icons ([5839bd2](https://gitlab.com/beaconsmind/client-sdk-flutter/commit/5839bd2af6311bc2bb32ecbe796fd08941733387))
* set min log level on native platforms ([a719905](https://gitlab.com/beaconsmind/client-sdk-flutter/commit/a719905430bf097ba1410fbd93dd4cafafcb8114))


### Bug Fixes

* **example:** call setLoggingEnabled with true ([323aa2a](https://gitlab.com/beaconsmind/client-sdk-flutter/commit/323aa2ae2db6452b3016b33452f9323ff1d5e9db))
* **example:** set correct badgeName parameter ([1ad07b1](https://gitlab.com/beaconsmind/client-sdk-flutter/commit/1ad07b1564d378105397e6205d3330b4f6b02680))
* **example:** support building android in release mode ([1a3a66d](https://gitlab.com/beaconsmind/client-sdk-flutter/commit/1a3a66d64b77d12f1e5b9bcc62dc10a0554cc1a7))
* iOS return result from startListeningBeacons ([19681ef](https://gitlab.com/beaconsmind/client-sdk-flutter/commit/19681ef2072fca3804793921bec419267db46acc))
* parseOfferId ([a8fd370](https://gitlab.com/beaconsmind/client-sdk-flutter/commit/a8fd370cffced6ed18ec89a133b146b18af160b7))
* pass required param ([838d6d8](https://gitlab.com/beaconsmind/client-sdk-flutter/commit/838d6d89229e31aa6cd73e968ad0750dde57d67d))


## 1.0.0

* Initial release
* Support Beaconsmind iOS SDK 3.3.0
* Support Beaconsmind Android SDK 1.4.1

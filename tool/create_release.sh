#!/bin/bash

# This script will:
# - generate RELEASE_NOTES.md && CHANGELOG.md files,
# - update the flutter package version in pubspec.yaml file,
# - commit changes with message 'chore(release): release x.y.z',
# - create tag x.y.z,
# - push commit and tag to remote.
#
# In order to run the script, make sure you have installed the following dependencies:
# - `fvm`. Checkout the https://fvm.app/ website on how to install it.
# - `git`. Install by running `brew install git`.
# - `standard-changelog`. Install by running `npm install -g standard-changelog`. Consider using `nvm` for managing node versions.
# - `standard-version`. Install by running `npm install -g standard-version`. Consider using `nvm` for managing node versions.
# - `yq`. Install by running `brew install yq`.

# Fetch the latest tags from the remote.
git fetch --all

# Generate RELEASE_NOTES.md file with only the changes since the last release.
rm RELEASE_NOTES.md && touch RELEASE_NOTES.md
standard-changelog -i RELEASE_NOTES.md -o RELEASE_NOTES.md -a false

# Run `standard-version`. It will:
# - update the CHANGELOG.md with changes from the latests release based on conventional commits,
# - commit changes in the CHANGELOG.md file,
# - create a tag based on the previous version and conventional commits, set the --tag-prefix to empty string to conform to Android & iOS SDKs tags.
standard-version --tag-prefix=""

# Get the tag that was generated by `standard-version`.
TAG=$(git describe --tags)

# Update version in the pubspec.yaml file.
yq eval '.version = "'$TAG'"' -i pubspec.yaml

# Run `fvm flutter packages get` in the example directory to update pubspec.lock file.
cd example && fvm flutter packages get && cd ..

# Commit all changes.
git add --all
git commit --amend --no-edit

# Recreate tag after commit
git tag --delete $TAG
git tag $TAG

# Push to remote.
git push && git push --tags§

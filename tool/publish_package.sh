#!/bin/bash

# This script will:
# - publish the package to pub.dev.
#
# In order to run the script, make sure you have installed the following dependencies:
# - `dart`. See instructions on how to install here:https://dart.dev/get-dart.

dart pub publish
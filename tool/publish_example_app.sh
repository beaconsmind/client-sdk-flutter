#!/bin/bash

# This script will:
# - build & publish the iOS example app to TestFlight
# - build Android example app.
# - TODO: publish `.ipa` * `.apk` to the corresponding GitLab Release.
#
# In order to run the script, make sure you have installed the following dependencies:
# - `flutter`. See instructions on how to install here: https://docs.flutter.dev/get-started/install.
# - `fastlane`. See instructions on how to install here: https://docs.fastlane.tools/getting-started/installation.

# Set the Apple Team ID.
APPLE_TEAM_ID="<APPLE_TEAM_ID>"

# Generate build number based on a number of commits from the beginning.
BUILD_NUMBER=$(git rev-list --count HEAD)

# Get the release notes from the RELEASE_NOTES.md file.
RELEASE_NOTES=$(cat RELEASE_NOTES.md)

# Read the current package version from the pubspec.yaml file.
VERSION=$(yq '.version' < pubspec.yaml)

# Change directory to the example app project.
cd example

# Build the Android example app.
flutter build apk --release --build-number $BUILD_NUMBER --build-name $VERSION

# TODO: publish it to a corresponding GitLab Release

# Build the iOS example app.
flutter build ipa --release --build-number $BUILD_NUMBER --build-name $VERSION

# TODO: publish it to a corresponding GitLab Release

# Publish the app to TestFlight using TestFlight.
fastlane run testflight \
    team_id:"$APPLE_TEAM_ID" \
    ipa:"build/ios/ipa/Runner.ipa" \
    changelog:"$RELEASE_NOTES"


# TODO: send slack success/error message.







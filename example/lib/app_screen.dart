// ignore_for_file: avoid_print

import 'dart:async';

import 'package:beaconsmind_flutter_demo/offers/offer_screen.dart';
import 'package:beaconsmind_flutter_demo/utils/app_permissions.dart';
import 'package:beaconsmind_sdk/beaconsmind_sdk.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:beaconsmind_flutter_demo/beacons/beacons_screen.dart';
import 'package:beaconsmind_flutter_demo/offers/offers_screen.dart';
import 'package:beaconsmind_flutter_demo/profile_tab.dart';

class AppScreen extends StatefulWidget {
  const AppScreen({Key? key}) : super(key: key);

  @override
  State<AppScreen> createState() => _AppScreenState();
}

class _AppScreenState extends State<AppScreen> with WidgetsBindingObserver {
  var _selectedIndex = 1;

  StreamSubscription<RemoteMessage>? _onMessageSub;
  StreamSubscription<RemoteMessage>? onMessageOpenedAppSub;
  StreamSubscription<String?>? _onTokenRefreshSub;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    _init();
  }

  Future<void> _init() async {
    /// Request notification permissions, set callbacks & save device token.
    await _setupNotifications();

    /// Request required permissions (location, bluetooth & notifications).
    await requestRequiredPermissions(context);

    /// If all permissions have been granted, start listening beacons.
    await _startListeningForBeaconIfRequiredPermissionsGranted();
  }

  Future<void> _startListeningForBeaconIfRequiredPermissionsGranted() async {
    /// If all permissions have been granted, start listening beacons.
    final permissionsGranted = await areRequiredPermissionsGranted();
    if (permissionsGranted) {
      Beaconsmind.instance.startListeningBeacons();
    }
  }

  Future<void> _setupNotifications() async {
    await FirebaseMessaging.instance.requestPermission();

    FirebaseMessaging.instance.getInitialMessage().then(_onMessage);
    _onMessageSub = FirebaseMessaging.onMessage.listen(_onMessage);
    onMessageOpenedAppSub =
        FirebaseMessaging.onMessageOpenedApp.listen(_onMessage);

    _getToken().then(_setDeviceToken);
    _onTokenRefreshSub =
        FirebaseMessaging.instance.onTokenRefresh.listen(_setDeviceToken);
  }

  /// If using APNS on iOS, make sure to set the platformType parameter
  /// to [PlatformType.apns].
  void _setDeviceToken(String? token) async {
    if (token != null) {
      try {
        await Beaconsmind.instance.registerDeviceToken(
          deviceToken: token,
          platformType: PlatformType.fcm,
        );
      } catch (e) {
        print(e);
      }
    }
  }

  /// If using APNS on iOS, you can obtain the APNS token by calling
  /// [FirebaseMessaging.instance.getAPNSToken()].
  Future<String?> _getToken() {
    return FirebaseMessaging.instance.getToken();
  }

  Future<void> _onMessage(RemoteMessage? message) async {
    if (message == null) {
      return;
    }

    try {
      final offerId = Beaconsmind.instance.parseOfferId(data: message.data)!;
      Beaconsmind.instance.markOfferAsReceived(offerId: offerId);

      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => OfferScreen(
            offerId: offerId,
            offer: null,
          ),
        ),
      );
    } catch (e) {
      debugPrint(e.toString());
      // This push is not from Beaconsmind
    }
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) async {
    /// Start listening for beacons on app resume to ensure beacon listening
    /// starts after obtaining permissions from phone settings scenario.
    if (state == AppLifecycleState.resumed) {
      _startListeningForBeaconIfRequiredPermissionsGranted();
    }
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    _onMessageSub?.cancel();
    onMessageOpenedAppSub?.cancel();
    _onTokenRefreshSub?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Builder(
        builder: (context) {
          switch (_selectedIndex) {
            case 0:
              return const BeaconsScreen();
            case 1:
              return const OffersScreen();
            case 2:
              return const ProfileTab();
            default:
              return const Text('App');
          }
        },
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.bluetooth),
            label: 'Beacons',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.local_offer),
            label: 'Offers',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.person),
            label: 'Profile',
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: Colors.amber[800],
        onTap: _onItemTapped,
      ),
    );
  }

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }
}

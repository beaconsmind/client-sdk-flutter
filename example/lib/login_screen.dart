import 'package:flutter/material.dart';
import 'package:beaconsmind_sdk/beaconsmind_sdk.dart';
import 'package:beaconsmind_flutter_demo/register_screen.dart';
import 'package:provider/provider.dart';

import 'beaconsmind_config.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  bool _loading = false;
  final _usernameController = TextEditingController();
  final _passController = TextEditingController();
  final _hostnameController = TextEditingController();

  @override
  void initState() {
    _hostnameController.text =
        context.read<BeaconsmindConfigChangeNotifier>().config.hostname;
    super.initState();
  }

  @override
  void dispose() {
    _usernameController.dispose();
    _passController.dispose();
    _hostnameController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (_loading) {
      return const Center(
        child: CircularProgressIndicator(),
      );
    }

    return Scaffold(
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16),
          child: Column(
            children: [
              const SizedBox(height: 32),
              TextFormField(
                controller: _usernameController,
                keyboardType: TextInputType.emailAddress,
                textCapitalization: TextCapitalization.none,
                autocorrect: false,
                decoration: const InputDecoration(labelText: 'Username'),
              ),
              const SizedBox(height: 16),
              TextFormField(
                controller: _passController,
                keyboardType: TextInputType.visiblePassword,
                obscureText: true,
                decoration: const InputDecoration(labelText: 'Password'),
              ),
              const SizedBox(height: 24),
              ElevatedButton(
                onPressed: () async {
                  setState(() {
                    _loading = true;
                  });
                  try {
                    await Beaconsmind.instance.login(
                      username: _usernameController.text,
                      password: _passController.text,
                    );
                  } catch (e) {
                    final snackBar = SnackBar(
                      content: Text(e.toString()),
                    );
                    ScaffoldMessenger.of(context).showSnackBar(snackBar);
                  }
                  setState(() {
                    _loading = false;
                  });
                  // NOTE: No need to handle user navigation here.
                  // The user will be redirected using the StreamBuilder on main.dart.
                },
                child: const Text(
                  'Login',
                ),
              ),
              const SizedBox(height: 16),
              TextButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => const RegisterScreen(),
                    ),
                  );
                },
                child: const Text('Create account'),
              ),
              const SizedBox(height: 32),
              TextFormField(
                controller: _hostnameController,
                textCapitalization: TextCapitalization.none,
                autocorrect: false,
                decoration:
                    const InputDecoration(labelText: 'Select environment'),
              ),
              const SizedBox(height: 16),
              ElevatedButton(
                onPressed: () {
                  final hostname = _hostnameController.text;

                  Beaconsmind.instance.updateHostname(hostname: hostname);

                  context
                      .read<BeaconsmindConfigChangeNotifier>()
                      .updateHostname(hostname);
                },
                child: const Text('Change environment'),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

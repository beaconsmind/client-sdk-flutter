import 'dart:async';

import 'package:flutter/material.dart';
import 'package:beaconsmind_sdk/beaconsmind_sdk.dart';
import 'package:flutter/services.dart';
import 'package:equatable/equatable.dart';
import 'package:timeago/timeago.dart' as timeago;

class BeaconsScreen extends StatefulWidget {
  const BeaconsScreen({Key? key}) : super(key: key);

  @override
  State<BeaconsScreen> createState() => _BeaconsScreenState();
}

class _BeaconsScreenState extends State<BeaconsScreen> {
  _BeaconsState _state = const _BeaconsInitialState();

  late final Timer _timer;

  @override
  void initState() {
    _fetchBeacons();

    _timer = Timer.periodic(const Duration(seconds: 1), (_) => _fetchBeacons());

    super.initState();
  }

  @override
  dispose() {
    _timer.cancel();
    super.dispose();
  }

  Future<void> _fetchBeacons() async {
    try {
      final beacons = await Beaconsmind.instance.getBeaconContactsSummary();

      if (mounted) {
        setState(() {
          _state = _BeaconsPopulatedState(beacons: beacons);
        });
      }
    } catch (error) {
      if (mounted) {
        setState(() {
          _state = _BeaconsErrorState(error: error);
        });
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: RefreshIndicator(
        onRefresh: () => _fetchBeacons(),
        child: _mapStateToView(_state),
      ),
    );
  }

  Widget _mapStateToView(_BeaconsState state) {
    if (state is _BeaconsInitialState) {
      return const _BeaconsInitialView();
    } else if (state is _BeaconsPopulatedState) {
      return _BeaconsPopulatedView(state: state);
    } else if (state is _BeaconsErrorState) {
      return _BeaconsErrorView(state: state);
    } else {
      return Container();
    }
  }
}

class _BeaconsPopulatedView extends StatelessWidget {
  const _BeaconsPopulatedView({
    Key? key,
    required this.state,
  }) : super(key: key);

  final _BeaconsPopulatedState state;

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      padding: const EdgeInsets.all(16),
      itemCount: state.beacons.length,
      itemBuilder: (context, index) {
        final beacon = state.beacons[index];
        return _BeaconListTile(beacon: beacon);
      },
    );
  }
}

class _BeaconListTile extends StatelessWidget {
  const _BeaconListTile({
    Key? key,
    required this.beacon,
  }) : super(key: key);

  final BeaconContactsSummary beacon;

  @override
  Widget build(BuildContext context) {
    return Card(
      color: beacon.isInRange ? Colors.green.shade100 : null,
      child: ListTile(
        isThreeLine: true,
        onTap: () {
          Clipboard.setData(ClipboardData(text: beacon.uuid));
          ScaffoldMessenger.of(context).showSnackBar(
            const SnackBar(
              duration: Duration(milliseconds: 300),
              content: Text('ID copied.'),
            ),
          );
        },
        title: Text(beacon.name),
        subtitle: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SizedBox(height: 8),
            Text('ID: ${beacon.uuid}'),
            Text('Major: ${beacon.major}, Minor: ${beacon.minor}'),
            const SizedBox(height: 8),
            Text(
              'Contacted ${timeago.format(
                DateTime.fromMillisecondsSinceEpoch(
                  beacon.lastContactTimestamp ?? 0,
                ),
              )}',
            ),
            Text(
              'Contacts: '
              '${beacon.contactsCount} times (${beacon.contactFrequencyPerMinute} per minute)',
            ),
            const SizedBox(height: 8),
            if (beacon.currentRSSI != null && beacon.averageRSSI != null)
              Text(
                'Signal: '
                '${beacon.currentRSSI!.toStringAsFixed(2)} dB '
                '(${beacon.averageRSSI!.toStringAsFixed(2)} dB average)',
              ),
            const SizedBox(height: 8),
          ],
        ),
      ),
    );
  }
}

class _BeaconsInitialView extends StatelessWidget {
  const _BeaconsInitialView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Center(
      child: Text('Waiting for connection from SDK'),
    );
  }
}

class _BeaconsErrorView extends StatelessWidget {
  const _BeaconsErrorView({
    Key? key,
    required this.state,
  }) : super(key: key);

  final _BeaconsErrorState state;

  @override
  Widget build(BuildContext context) {
    return Text(state.error.toString());
  }
}

abstract class _BeaconsState extends Equatable {
  const _BeaconsState();
}

class _BeaconsPopulatedState extends _BeaconsState {
  const _BeaconsPopulatedState({
    required this.beacons,
  }) : super();

  final List<BeaconContactsSummary> beacons;

  @override
  List<Object> get props => [beacons];
}

class _BeaconsInitialState extends _BeaconsState {
  const _BeaconsInitialState() : super();

  @override
  List<Object> get props => [];
}

class _BeaconsErrorState extends _BeaconsState {
  const _BeaconsErrorState({
    required this.error,
  }) : super();

  final Object error;

  @override
  List<Object> get props => [error];
}

import 'dart:io';

import 'package:flutter/material.dart';
import 'package:permission_handler/permission_handler.dart';

Future<bool> areRequiredPermissionsGranted() async {
  final permissionsStatuses = await Future.wait([
    /// BluetoothScan permission is applicable to Android only.
    if (Platform.isAndroid) Permission.bluetoothScan.status,
    Permission.notification.status,
    Permission.locationWhenInUse.status,
  ]);

  return permissionsStatuses.every((permission) {
    return permission.isGranted;
  });
}

Future<void> requestRequiredPermissions(BuildContext context) async {
  await _requestNotificationPermission(context);
  await _requestBluetoothScanPermission(context);
  await _requestLocationWhenInUsePermission(context);
  await _requestLocationAlwaysPermission(context);
}

Future<void> _requestNotificationPermission(BuildContext context) async {
  const permission = Permission.notification;

  PermissionStatus permissionStatus = await permission.status;

  if (permissionStatus == PermissionStatus.denied) {
    permissionStatus = await permission.request();

    if (permissionStatus == PermissionStatus.denied) {
      await showPermissionDialog(
        context: context,
        title: 'Notifications permission',
        content:
            'This app needs notification permission to send you exclusive offers notifications.',
        buttonText: 'Enable',
        onTap: () async {
          permissionStatus = await permission.request();
          Navigator.of(context).pop();
        },
      );
    }
  }

  if (permissionStatus == PermissionStatus.permanentlyDenied) {
    await showPermissionDialog(
      context: context,
      title: 'Notification permission denied',
      content:
          'Without notification permissions you won\'t get exclusive offers notifications. '
          'Please grant notification permissions in your settings',
      buttonText: 'Open settings',
      onTap: () async {
        await openAppSettings();
        Navigator.of(context).pop();
      },
    );
  }
}

Future<void> _requestBluetoothScanPermission(BuildContext context) async {
  if (!Platform.isAndroid) {
    return;
  }

  const permission = Permission.bluetoothScan;

  PermissionStatus permissionStatus = await permission.status;

  if (permissionStatus == PermissionStatus.denied) {
    permissionStatus = await permission.request();

    if (permissionStatus == PermissionStatus.denied) {
      await showPermissionDialog(
        context: context,
        title: 'Bluetooth location',
        content:
            'This app needs bluetooth permission to provide you with exclusive offers even when you are not in the app.',
        buttonText: 'Enable',
        onTap: () async {
          permissionStatus = await permission.request();
          Navigator.of(context).pop();
        },
      );
    }
  }

  if (permissionStatus == PermissionStatus.permanentlyDenied) {
    await showPermissionDialog(
      context: context,
      title: 'Bluetooth permissions denied',
      content: 'Without bluetooth permissions you won\'t get exclusive offers. '
          'Please grant bluetooth permissions in your settings',
      buttonText: 'Open settings',
      onTap: () async {
        await openAppSettings();
        Navigator.of(context).pop();
      },
    );
  }
}

Future<void> _requestLocationWhenInUsePermission(BuildContext context) async {
  const permission = Permission.locationWhenInUse;

  PermissionStatus permissionStatus = await permission.status;

  if (permissionStatus == PermissionStatus.denied) {
    permissionStatus = await permission.request();

    if (permissionStatus == PermissionStatus.denied) {
      await showPermissionDialog(
        context: context,
        title: 'Location permissions',
        content:
            'This app needs location permissions to provide you with exclusive offers.',
        buttonText: 'Enable',
        onTap: () async {
          permissionStatus = await permission.request();
          Navigator.of(context).pop();
        },
      );
    }
  }

  if (permissionStatus == PermissionStatus.permanentlyDenied) {
    await showPermissionDialog(
      context: context,
      title: 'Location permissions denied',
      content: 'Without location permissions you won\'t get exclusive offers. '
          'Please grant location permissions in your settings',
      buttonText: 'Open settings',
      onTap: () async {
        await openAppSettings();
        Navigator.of(context).pop();
      },
    );
  }
}

Future<void> _requestLocationAlwaysPermission(BuildContext context) async {
  final locationWhenInUseStatus = await Permission.locationWhenInUse.status;
  if (locationWhenInUseStatus != PermissionStatus.granted) {
    return;
  }

  const permission = Permission.locationAlways;

  PermissionStatus permissionStatus = await permission.status;

  if (permissionStatus == PermissionStatus.denied) {
    permissionStatus = await permission.request();

    if (permissionStatus == PermissionStatus.denied) {
      await showPermissionDialog(
        context: context,
        title: 'Background location',
        content:
            'This app needs background location permission to provide you with exclusive offers even when you are not in the app.',
        buttonText: 'Enable',
        onTap: () async {
          permissionStatus = await permission.request();
          Navigator.of(context).pop();
        },
      );
    }
  }

  if (permissionStatus == PermissionStatus.permanentlyDenied) {
    await showPermissionDialog(
      context: context,
      title: 'Background location permissions denied',
      content:
          'Without background location permissions you won\'t get exclusive offers when you are not in the app. '
          'Please enable it in in your phone\'s settings',
      buttonText: 'Open settings',
      onTap: () async {
        await openAppSettings();
        Navigator.of(context).pop();
      },
    );
  }
}

Future<void> showPermissionDialog({
  required BuildContext context,
  required String title,
  required String content,
  required String buttonText,
  required VoidCallback onTap,
}) async {
  await showDialog(
    context: context,
    builder: (context) {
      return AlertDialog(
        title: Text(title),
        content: Text(content),
        actions: [
          TextButton(
            onPressed: () async {
              onTap();
            },
            child: Text(buttonText),
          ),
        ],
      );
    },
  );
}

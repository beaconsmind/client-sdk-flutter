import 'package:flutter/material.dart';
import 'package:beaconsmind_sdk/beaconsmind_sdk.dart';

class RegisterScreen extends StatefulWidget {
  const RegisterScreen({Key? key}) : super(key: key);

  @override
  State<RegisterScreen> createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  bool _loading = false;
  final _firstNameController = TextEditingController();
  final _lastNameController = TextEditingController();
  final _emailController = TextEditingController();
  final _countryController = TextEditingController();
  final _passController = TextEditingController();
  final _passConfirmController = TextEditingController();

  @override
  void dispose() {
    _firstNameController.dispose();
    _lastNameController.dispose();
    _emailController.dispose();
    _countryController.dispose();
    _passController.dispose();
    _passConfirmController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (_loading) {
      return const Center(
        child: CircularProgressIndicator(),
      );
    }

    return Scaffold(
      appBar: AppBar(
        title: const Text(
          'Signup',
        ),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              TextFormField(
                controller: _firstNameController,
                keyboardType: TextInputType.name,
                textCapitalization: TextCapitalization.words,
                autocorrect: false,
                decoration: const InputDecoration(labelText: 'First Name'),
              ),
              const SizedBox(height: 16),
              TextFormField(
                controller: _lastNameController,
                keyboardType: TextInputType.name,
                textCapitalization: TextCapitalization.words,
                autocorrect: false,
                decoration: const InputDecoration(labelText: 'Last Name'),
              ),
              const SizedBox(height: 16),
              TextFormField(
                controller: _emailController,
                keyboardType: TextInputType.emailAddress,
                textCapitalization: TextCapitalization.none,
                autocorrect: false,
                decoration: const InputDecoration(labelText: 'Email'),
              ),
              const SizedBox(height: 16),

              /// TODO(korzonkiee): It should be converted to a dropdown.
              /// The list of country codes can be fetched from the SDK.
              /// https://beaconsmind.monday.com/boards/2736418117/views/87183611/pulses/4549533025
              TextFormField(
                controller: _countryController,
                maxLength: 3,
                decoration: const InputDecoration(
                  labelText: 'Country (3 letter code, e.g. CHE, DEU, POL, SAU)',
                ),
              ),
              const SizedBox(height: 16),
              TextFormField(
                controller: _passController,
                keyboardType: TextInputType.visiblePassword,
                textCapitalization: TextCapitalization.none,
                autocorrect: false,
                obscureText: true,
                decoration: const InputDecoration(labelText: 'Password'),
              ),
              const SizedBox(height: 16),
              TextFormField(
                controller: _passConfirmController,
                keyboardType: TextInputType.visiblePassword,
                textCapitalization: TextCapitalization.none,
                autocorrect: false,
                obscureText: true,
                decoration:
                    const InputDecoration(labelText: 'Confirm Password'),
              ),
              const SizedBox(height: 24),
              ElevatedButton(
                onPressed: () async {
                  setState(() {
                    _loading = true;
                  });
                  try {
                    await Beaconsmind.instance.signup(
                      username: _emailController.text,
                      firstName: _firstNameController.text,
                      lastName: _lastNameController.text,
                      password: _passController.text,
                      confirmPassword: _passConfirmController.text,
                      countryCode: _countryController.text,
                    );
                    Navigator.of(context).pop();
                  } catch (e) {
                    final snackBar = SnackBar(
                      content: Text(e.toString()),
                    );
                    ScaffoldMessenger.of(context).showSnackBar(snackBar);
                  }
                  setState(() {
                    _loading = false;
                  });
                  // NOTE: No need to handle user navigation here.
                  // The user will be redirected using the StreamBuilder on main.dart.
                },
                child: const Text(
                  'Signup',
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

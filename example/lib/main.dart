// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:beaconsmind_flutter_demo/app_screen.dart';
import 'package:beaconsmind_flutter_demo/login_screen.dart';
import 'package:beaconsmind_sdk/beaconsmind_sdk.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'beaconsmind_config.dart';

Future<void> _firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  try {
    await Firebase.initializeApp();
    WidgetsFlutterBinding.ensureInitialized();
    Beaconsmind.instance.markOfferAsReceived(
      offerId: Beaconsmind.instance.parseOfferId(data: message.data)!,
    );
  } catch (e) {
    debugPrint(e.toString());
    // This push is not from Beaconsmind
  }
}

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);

  runApp(const MyMaterialApp());
}

class MyMaterialApp extends StatelessWidget {
  const MyMaterialApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) => BeaconsmindConfigChangeNotifier(
        config: BeaconsmindConfig(
          hostname: 'https://test-develop-suite.azurewebsites.net',
        ),
      ),
      child: const MaterialApp(
        title: 'Beaconsmind',
        home: MyApp(),
      ),
    );
  }
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  Stream<BeaconsmindSdkContext?>? contextEvents;

  @override
  void initState() {
    super.initState();
    initialize();
  }

  Future<void> initialize() async {
    await Beaconsmind.instance.setMinLogLevel(LogLevel.debug);

    const startOptions = StartOptions(
      android: AndroidStartOptions(
        usePassiveScanning: true,
        notificationOptions: AndroidNotificationOptions(
          badgeName: 'ic_beacons',
          title: 'Beaconsmind sdk demo',
          text: 'Listening to beacons',
          channelName: 'beaconsmind',
        ),
      ),
    );

    final beaconsmindConfig =
        context.read<BeaconsmindConfigChangeNotifier>().config;

    await Beaconsmind.instance.initializeDevelop(
      hostname: beaconsmindConfig.hostname,
      appVersion: '0.0.1',
      options: startOptions,
    );

    contextEvents = Beaconsmind.instance.contextEvents();
    if (mounted) {
      setState(() {});
    }
  }

  @override
  void dispose() {
    Beaconsmind.instance.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Beaconsmind example app'),
      ),
      body: StreamBuilder<BeaconsmindSdkContext?>(
        stream: contextEvents,
        builder: (BuildContext context,
            AsyncSnapshot<BeaconsmindSdkContext?> snapshot) {
          if (snapshot.hasError) {
            return Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text('Error: ${snapshot.error.toString()}'),
                  ElevatedButton(
                    onPressed: () {
                      initialize();
                    },
                    child: const Text('Try again'),
                  ),
                ],
              ),
            );
          } else {
            switch (snapshot.connectionState) {
              case ConnectionState.none:
                return Center(
                  child: ElevatedButton(
                    onPressed: () {
                      initialize();
                    },
                    child: const Text('Initialize SDK'),
                  ),
                );
              case ConnectionState.waiting:
                return const Center(
                  child: Text('Waiting for connection from SDK'),
                );
              case ConnectionState.active:
                if (snapshot.data == null) {
                  return const LoginScreen();
                } else {
                  return const AppScreen();
                }
              case ConnectionState.done:
                return Center(
                  child: ElevatedButton(
                    onPressed: () {
                      initialize();
                    },
                    child: const Text('Initialize SDK again'),
                  ),
                );
            }
          }
        },
      ),
    );
  }
}

// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:flutter/material.dart';

class BeaconsmindConfig {
  final String hostname;

  BeaconsmindConfig({
    required this.hostname,
  });

  BeaconsmindConfig copyWith({
    String? hostname,
  }) {
    return BeaconsmindConfig(
      hostname: hostname ?? this.hostname,
    );
  }

  @override
  bool operator ==(covariant BeaconsmindConfig other) {
    if (identical(this, other)) return true;

    return other.hostname == hostname;
  }

  @override
  int get hashCode => hostname.hashCode;
}

class BeaconsmindConfigChangeNotifier extends ChangeNotifier {
  BeaconsmindConfigChangeNotifier({
    required BeaconsmindConfig config,
  }) : _config = config;

  BeaconsmindConfig _config;
  BeaconsmindConfig get config => _config;

  void updateHostname(String hostname) {
    _config = _config.copyWith(hostname: hostname);
    notifyListeners();
  }
}

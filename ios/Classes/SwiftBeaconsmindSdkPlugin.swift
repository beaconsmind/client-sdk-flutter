import Flutter
import UIKit
import Beaconsmind
import CoreLocation

public class SwiftBeaconsmindSdkPlugin: NSObject, FlutterPlugin {
    private var eventsSink: FlutterEventSink?
    
    public static func register(with registrar: FlutterPluginRegistrar) {
        let methods = FlutterMethodChannel(name: "beaconsmind/methods", binaryMessenger: registrar.messenger())
        let contextEvents = FlutterEventChannel(name: "beaconsmind/contextEvents", binaryMessenger: registrar.messenger())
        
        let instance = SwiftBeaconsmindSdkPlugin()
        registrar.addMethodCallDelegate(instance, channel: methods)
        contextEvents.setStreamHandler(instance)
    }
    
    public func handle(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
        if call.method == "initialize" {
            guard let args = call.arguments as? [String: Any],
                  let appVersion = args["appVersion"] as? String,
                  let hostname = args["hostname"] as? String else {
                      result(FlutterError.init(code : "500", message: "Invalid arguments!", details: nil))
                      return
                  }
            do {
                try
                Beaconsmind.default.start(
                    delegate: self,
                    appVersion: appVersion,
                    hostname: hostname
                )
                result(true)
            } catch {
                result(FlutterError.init(code: "500", message: "initialize failed!", details: error.localizedDescription))
            }
        }
        if call.method == "initializeDevelop" {
            guard let args = call.arguments as? [String: Any],
                  let appVersion = args["appVersion"] as? String,
                  let hostname = args["hostname"] as? String else {
                      result(FlutterError.init(code : "500", message: "Invalid arguments!", details: nil))
                      return
                  }
            do {
                try
                Beaconsmind.default.startDevelop(
                    delegate: self,
                    appVersion: appVersion,
                    hostname: hostname
                )
                result(true)
            } catch {
                result(FlutterError.init(code: "500", message: "initializeDevelop failed!", details: error.localizedDescription))
            }
        }
        //
        // MARK: start
        // Deprecated, use initialize instead
        //
        if call.method == "start" {
            guard let args = call.arguments as? [String: Any],
                  let appVersion = args["appVersion"] as? String,
                  let hostname = args["hostname"] as? String else {
                      result(FlutterError.init(code : "500", message: "Invalid arguments!", details: nil))
                      return
                  }
            do {
                try
                Beaconsmind.default.start(
                    delegate: self,
                    appVersion: appVersion,
                    hostname: hostname
                )
                result(true)
            } catch {
                result(FlutterError.init(code: "500", message: "start failed!", details: error.localizedDescription))
            }
        }
        //
        // MARK: Authentication
        //
        
        else if call.method == "signup" {
            guard let args = call.arguments as? [String: Any]
            else {
                result(FlutterError.init(code : "500", message: "Invalid arguments!", details: nil))
                return
            }
            do {
                let username = args["username"] as! String
                let firstName = args["firstName"] as! String
                let lastName = args["lastName"] as! String
                let password = args["password"] as! String
                let confirmPassword = args["confirmPassword"] as! String
                let language = args["language"] as? String
                let gender = args["gender"] as? String
                let favoriteStoreID = args["favoriteStoreID"] as? Int
                let birthDateSeconds = args["birthDateSeconds"] as? Double
                let countryCode = args["countryCode"] as? String
                
                try
                Beaconsmind.default.signup(
                    username: username,
                    firstName: firstName,
                    lastName: lastName,
                    password: password,
                    confirmPassword: confirmPassword,
                    language: language,
                    gender: gender,
                    favoriteStoreID: favoriteStoreID,
                    birthDate:  getDateDayFromSeconds(seconds: birthDateSeconds),
                    countryCode: countryCode
                ) { resp in
                        switch resp {
                        case let .success(ctx):
                            result([
                                "userId": ctx.userID,
                            ])
                        case let .failure(error):
                            result(FlutterError.init(code : "500", message: "signup failed!", details: error.errorDescription))
                        }
                    }
            }  catch  {
                result(FlutterError.init(code: "500", message: "signup failed!", details: error.localizedDescription))
            }
        }
        
        else if call.method == "login" {
            guard let args = call.arguments as? [String: Any],
                  let username = args["username"] as? String,
                  let password = args["password"] as? String
            else {
                result(FlutterError.init(code : "500", message: "Invalid arguments!", details: nil))
                return
            }
            do {
                try
                Beaconsmind.default.login(
                    username: username,
                    password: password) { resp in
                        switch resp {
                        case let .success(ctx):
                            result([
                                "userId": ctx.userID,
                            ])
                        case let .failure(error):
                            result(FlutterError.init(code : "500", message: "login failed!", details: error.errorDescription))
                        }
                    }
            }  catch  {
                result(FlutterError.init(code: "500", message: "login failed!", details: error.localizedDescription))
            }
        }
        
        else if call.method == "importAccount" {
            guard let args = call.arguments as? [String: Any],
                  let id = args["id"] as? String,
                  let email = args["email"] as? String,
                  let firstName = args["firstName"] as? String?,
                  let lastName = args["lastName"] as? String?,
                  let language = args["language"] as? String?,
                  let gender = args["gender"] as? String?,
                  let birthDateSeconds = args["birthDateSeconds"] as? Double?
            else {
                result(FlutterError.init(code : "500", message: "Invalid arguments!", details: nil))
                return
            }
            
            do {
                try
                Beaconsmind.default.importAccount(
                    id: id,
                    email: email,
                    firstName: firstName,
                    lastName: lastName,
                    birthDate:  getDateDayFromSeconds(seconds: birthDateSeconds)?.date,
                    language: language,
                    gender: gender) { resp in
                        switch resp {
                        case let .success(ctx):
                            result([
                                "userId": ctx.userID,
                            ])
                        case let .failure(error):
                            result(FlutterError.init(code : "500", message: "importAccount failed!", details: error.errorDescription))
                        }
                    }
            }  catch  {
                result(FlutterError.init(code: "500", message: "importAccount failed!", details: error.localizedDescription))
            }
        }
        //
        // Profile
        //
        else if call.method == "getProfile" {
            do {
                try
                _ = Beaconsmind.default.getProfile() { resp in
                    switch resp {
                    case let .success(profile):
                        let encoder = JSONEncoder()
                        encoder.dateEncodingStrategy = .formatted(API.dateEncodingFormatter)
                        let jsonData = try! encoder.encode(profile)
                        let jsonValue = try! JSONSerialization.jsonObject(with: jsonData)
                        result(jsonValue)
                    case let .failure(error):
                        result(FlutterError.init(code: "500", message: "getProfile failed!", details: error.errorDescription))
                    }
                }
            }  catch  {
                result(FlutterError.init(code: "500", message: "getProfile failed!", details: error.localizedDescription))
            }
        }
        else if call.method == "updateProfile" {
            
            guard let args = call.arguments as? [String: Any]
            else {
                result(FlutterError.init(code : "500", message: "Invalid arguments!", details: nil))
                return
            }
            do {
                let firstName = args["firstName"] as! String
                let lastName = args["lastName"] as! String
                let birthDateSeconds = args["birthDateSeconds"] as? Double
                let city = args["city"] as? String
                let country = args["country"] as? String
                let disablePushNotifications = args["disablePushNotifications"] as? Bool
                let favoriteStoreID = args["favoriteStoreID"] as? Int
                let gender = args["gender"] as? String
                let houseNumber = args["houseNumber"] as? String
                let landlinePhone = args["landlinePhone"] as? String
                let language = args["language"] as? String
                let phoneNumber = args["phoneNumber"] as? String
                let street = args["street"] as? String
                let zipCode = args["zipCode"] as? String
                
                try
                _ = Beaconsmind.default.updateProfile(
                    firstName: firstName,
                    lastName: lastName,
                    birthDate:  getDateDayFromSeconds(seconds: birthDateSeconds)?.date,
                    city: city,
                    country: country,
                    disablePushNotifications: disablePushNotifications,
                    favoriteStoreID: favoriteStoreID,
                    gender: gender,
                    houseNumber: houseNumber,
                    landlinePhone: landlinePhone,
                    language: language,
                    phoneNumber: phoneNumber,
                    street: street,
                    zipCode: zipCode) { resp in
                        switch resp {
                        case let .success(profile):
                            let encoder = JSONEncoder()
                            encoder.dateEncodingStrategy = .formatted(API.dateEncodingFormatter)
                            let jsonData = try! encoder.encode(profile)
                            let jsonValue = try! JSONSerialization.jsonObject(with: jsonData)
                            result(jsonValue)
                            break
                        case let .failure(error):
                            result(FlutterError.init(code: "500", message: "updateProfile failed!", details: error.errorDescription))
                        }
                    }
            } catch {
                result(FlutterError.init(code: "500", message: "updateProfile failed!", details: error.localizedDescription))
            }
        }
        //
        // helpers
        //
        else  if call.method == "logout" {
            do {
                try Beaconsmind.default.logout()
                result(true)
            } catch {
                result(FlutterError.init(code: "500", message: "logout failed!", details: error.localizedDescription))
            }
        }
        
        else if call.method == "getAPIContext" {
            let ctx = Beaconsmind.default.getAPIContext()
            if ctx != nil {
                result([
                    "userId": ctx!.userID,
                ])
            } else {
                result(nil)
            }
            
        }
        else  if call.method == "updateHostname" {
            guard let args = call.arguments as? [String: Any],
                  let hostname = args["hostname"] as? String
            else {
                result(FlutterError.init(code : "500", message: "Invalid arguments!", details: nil))
                return
            }
            Beaconsmind.default.updateHostname(hostname: hostname)
            result(true)
        }
        //
        // Beacons
        //
        else if call.method == "startListeningBeacons" {
            do {
                try Beaconsmind.default.startListeningBeacons()
                result(true)
            } catch {
                result(FlutterError.init(code: "500", message: "startListeningBeacons failed!", details: error.localizedDescription))
            }
        }
        else  if call.method == "stopListeningBeacons" {
            Beaconsmind.default.stopListeningBeacons()
            result(true)
        }
        else if call.method == "getBeaconsSummary" {
            do {
                let request = API.Configuration.ConfigurationGetBeacons.Request()
                _ = try Beaconsmind.default.apiRequest(request) { res in
                    switch res {
                    case let .success(response):
                        let encoder = JSONEncoder()
                        encoder.dateEncodingStrategy = .formatted(API.dateEncodingFormatter)
                        let jsonData = try! encoder.encode(response.success)
                        let jsonString = String(data: jsonData, encoding: .utf8)
                        result(jsonString)
                    case let .failure(error):
                        result(FlutterError.init(code: "500", message: "getBeaconsSummary failed!", details: error.localizedDescription))
                    }
                }
            } catch {
                result(FlutterError.init(code: "500", message: "getBeaconsSummary failed!", details: error.localizedDescription))
            }
        }
        else if call.method == "getBeaconContactsSummary" {
            do {
                let beaconContactsSummary = Beaconsmind.default.beaconContactsSummaries
                let list = beaconContactsSummary.map { summary -> NSDictionary in
                    return [
                        "uuid": summary.uuid,
                        "major": summary.major,
                        "minor": summary.minor,
                        "name": summary.name,
                        "store": summary.store,
                        "currentRSSI": summary.rssi,
                        "averageRSSI": summary.averageRSSI,
                        "lastContactTimestamp": summary.timestamp,
                        "contactFrequencyPerMinute": summary.frequency,
                        "contactsCount": summary.contactCount,
                        "isInRange": summary.isInRange,
                    ]
                }
                result(list)
            } catch {
                result(FlutterError.init(code: "500", message: "getBeaconContactsSummary failed!", details: error.localizedDescription))
            }
        }
        else if call.method == "registerDeviceToken" {
            guard let args = call.arguments as? [String: Any],
                  let deviceToken = args["deviceToken"] as? String,
                  let platformType = args["platformType"] as? Int else {
                      result(FlutterError.init(code : "500", message: "Invalid arguments!", details: nil))
                      return
                  }
            do {
              try Beaconsmind.default.register(
                deviceToken: deviceToken,
                platformType: PlatformType.init(rawValue: platformType)!
              ) { resp in
                    switch resp {
                    case .success:
                        result(true)
                    case let .failure(error):
                        result(FlutterError.init(code: "500", message: "register failed!", details: error.errorDescription))
                    }
                }
            } catch {
                result(FlutterError.init(code: "500", message: "register failed!", details: error.localizedDescription))
            }
        }
        //
        // Offers
        //
        else if call.method == "markOfferAsRead" {
            guard let args = call.arguments as? [String: Any],
                  let offerId = args["offerId"] as? Int
            else {
                result(FlutterError.init(code : "500", message: "Invalid arguments!", details: nil))
                return
            }
            do {
                try Beaconsmind.default.markOfferAsRead(offerID: offerId) { resp in
                    switch resp {
                    case .success:
                        result(true)
                    case let .failure(error):
                        result(FlutterError.init(code: "500", message: "markOfferAsRead failed!", details: error.errorDescription))
                    }
                }
            } catch {
                result(FlutterError.init(code: "500", message: "markOfferAsRead failed!", details: error.localizedDescription))
            }
        }
        else   if call.method == "markOfferAsReceived" {
            guard let args = call.arguments as? [String: Any],
                  let offerId = args["offerId"] as? Int
            else {
                result(FlutterError.init(code : "500", message: "Invalid arguments!", details: nil))
                return
            }
            do {
                try Beaconsmind.default.markOfferAsReceived(offerID: offerId) { resp in
                    switch resp {
                    case .success:
                        result(true)
                    case let .failure(error):
                        result(FlutterError.init(code: "500", message: "markOfferAsReceived failed!", details: error.errorDescription))
                    }
                }
            } catch {
                result(FlutterError.init(code: "500", message: "markOfferAsReceived failed!", details: error.localizedDescription))
            }
        }
        else if call.method == "markOfferAsRedeemed" {
            guard let args = call.arguments as? [String: Any],
                  let offerId = args["offerId"] as? Int
            else {
                result(FlutterError.init(code : "500", message: "Invalid arguments!", details: nil))
                return
            }
            do {
                try Beaconsmind.default.markOfferAsRedeemed(offerID: offerId) { resp in
                    switch resp {
                    case .success:
                        result(true)
                    case let .failure(error):
                        result(FlutterError.init(code: "500", message: "markOfferAsRedeemed failed!", details: error.errorDescription))
                    }
                }
            } catch {
                result(FlutterError.init(code: "500", message: "markOfferAsRedeemed failed!", details: error.localizedDescription))
            }
        }
        else if (call.method == "loadOffer") {
            guard let args = call.arguments as? [String: Any],
                  let offerId = args["offerId"] as? Int
            else {
                result(FlutterError.init(code : "500", message: "Invalid arguments!", details: nil))
                return
            }
            do {
                let request = API.Offers.OffersGetOffer.Request(offerId: offerId)
                _ = try Beaconsmind.default.apiRequest(request) { res in
                    switch res {
                    case let .success(response):
                        let encoder = JSONEncoder()
                        encoder.dateEncodingStrategy = .formatted(API.dateEncodingFormatter)
                        let jsonData = try! encoder.encode(response.success)
                        let jsonString = String(data: jsonData, encoding: .utf8)
                        result(jsonString)
                    case let .failure(error):
                        result(FlutterError.init(code: "500", message: "loadOffers failed!", details: error.localizedDescription))
                    }
                }
            } catch {
                result(FlutterError.init(code: "500", message: "loadOffer failed!", details: error.localizedDescription))
            }
        }
        else if(call.method == "loadOffers") {
            do {
                let request = API.Offers.OffersGetOffers.Request()
                _ = try Beaconsmind.default.apiRequest(request) { res in
                    switch res {
                    case let .success(response):
                        let encoder = JSONEncoder()
                        encoder.dateEncodingStrategy = .formatted(API.dateEncodingFormatter)
                        let jsonData = try! encoder.encode(response.success)
                        let jsonString = String(data: jsonData, encoding: .utf8)
                        result(jsonString)
                    case let .failure(error):
                        result(FlutterError.init(code: "500", message: "loadOffers failed!", details: error.localizedDescription))
                    }
                }
            } catch {
                result(FlutterError.init(code: "500", message: "loadOffers failed!", details: error.localizedDescription))
            }
        }
        else if(call.method == "setMinLogLevel") {
            guard let args = call.arguments as? [String: Any],
                  let minLogLevelString = args["minLogLevel"] as? String
            else {
                result(FlutterError.init(code : "500", message: "Invalid arguments!", details: nil))
                return
            }
            
            let minLogLevel = LogLevel.init(rawValue: minLogLevelString) ?? LogLevel.silent
            Beaconsmind.default.setMinLogLevel(level: minLogLevel)
            
            result(true)
        }
        //
        // MARK: unimplemented
        //
        else {
            result(FlutterError.init(code: "404", message: "Methood \(call.method) unimplemented!", details: nil))
        }
    }
    
    func onMainThread(_ closure: @escaping () -> Void) {
        if Thread.isMainThread {
            closure()
        } else {
            DispatchQueue.main.async {
                closure()
            }
        }
    }
    
    private func getDateDayFromSeconds(seconds: Double?) -> DateDay? {
        var d: DateDay? = nil
        if seconds != nil {
            d = DateDay(date: Date(timeIntervalSince1970: seconds! ))
        }
        return d;
    }
}


extension SwiftBeaconsmindSdkPlugin: FlutterStreamHandler {
    public func onListen(withArguments arguments: Any?, eventSink sink: @escaping FlutterEventSink) -> FlutterError? {
        if(eventsSink == nil) {
            eventsSink = sink;
        }
        let context = Beaconsmind.default.getAPIContext()
        if (context != nil) {
            eventsSink?([
                "userId": context!.userID,
            ])
        } else {
            eventsSink?(nil)
        }
        return nil
    }
    
    public func onCancel(withArguments arguments: Any?) -> FlutterError? {
        eventsSink = nil
        return nil
    }
}

extension SwiftBeaconsmindSdkPlugin: BeaconsmindDelegate {
    public func beaconsmind(_ beaconsmind: Beaconsmind, onContextChanged context: APIContext?) {
        if context != nil {
            onMainThread { [unowned self] in
                self.eventsSink?([
                    "userId": context!.userID,
                ])
            }
        } else {
            onMainThread { [unowned self] in
                self.eventsSink?(nil)
            }
        }
    }
}
